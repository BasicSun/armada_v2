unit uPathes;

// !!! ������ �� ������ ��������� �� ������ ������ ��������

interface

(*******************************************************************************
���� � ��������� ������ �������.

��� ��� ������������:

*******************************************************************************)
type
  TrPathes = record
    // ������
    RootRemote      : string;
    RootLocal       : string;
    Input           : string;
    Output          : string;
    OutVArchiv      : string;
    OutEMail        : string;
    // �� �������������
    OutToConfirm    : string;
    OurFiles        : string;
    In_1C_image     : string;
    Error           : string;
    // ��� �������� � ������� UchMag
    OutUchMag       : string;
    // ��� �������� ���
    OutSMS          : string;
    Reports         : string;
    Save            : string;
    // ������
    Monarch         : string;
    CashOnDelivery  : string;
    Arc_Fregat      : string;
    // ����� �������
    FFiles          : string;
    Data            : string;
    ReportsLocal    : string;
    Picture         : string;
    TemplatesRemote : string;
  end;

function PathesToStr(const APathes: TrPathes): WideString;
function StrToPathes(const AValue: WideString): TrPathes;

implementation

uses
  SysUtils, Classes;

function PathesToStr(const APathes: TrPathes): WideString;
var List: TStringList;
begin
  List := TStringList.Create;
  try
    List.Values['RootRemote']         := APathes.RootRemote     ;
    List.Values['RootLocal']          := APathes.RootLocal      ;
    List.Values['Input']              := APathes.Input          ;
    List.Values['Output']             := APathes.Output         ;
    List.Values['OutVArchiv']         := APathes.OutVArchiv     ;
    List.Values['OutEMail']           := APathes.OutEMail       ;
    List.Values['OutToConfirm']       := APathes.OutToConfirm   ;
    List.Values['OurFiles']           := APathes.OurFiles       ;
    List.Values['In_1C_image']        := APathes.In_1C_image    ;
    List.Values['Error']              := APathes.Error          ;
    List.Values['OutUchMag']          := APathes.OutUchMag      ;
    List.Values['OutSMS']             := APathes.OutSMS         ;
    List.Values['Reports']            := APathes.Reports        ;
    List.Values['Save']               := APathes.Save           ;
    List.Values['Monarch']            := APathes.Monarch        ;
    List.Values['CashOnDelivery']     := APathes.CashOnDelivery ;
    List.Values['Arc_Fregat']         := APathes.Arc_Fregat     ;
    List.Values['FFiles']             := APathes.FFiles         ;
    Result := List.CommaText;
  finally
    FreeAndNil(List);
  end;
end;

function StrToPathes(const AValue: WideString): TrPathes;
var List: TStringList;
begin
  List := TStringList.Create;
  try
    List.CommaText := AValue;
    Result.RootRemote     := List.Values['RootRemote']     ;
    Result.RootLocal      := List.Values['RootLocal']      ;
    Result.Input          := List.Values['Input']          ;
    Result.Output         := List.Values['Output']         ;
    Result.OutVArchiv     := List.Values['OutVArchiv']     ;
    Result.OutEMail       := List.Values['OutEMail']       ;
    Result.OutToConfirm   := List.Values['OutToConfirm']   ;
    Result.OurFiles       := List.Values['OurFiles']       ;
    Result.In_1C_image    := List.Values['In_1C_image']    ;
    Result.Error          := List.Values['Error']          ;
    Result.OutUchMag      := List.Values['OutUchMag']      ;
    Result.OutSMS         := List.Values['OutSMS']         ;
    Result.Reports        := List.Values['Reports']        ;
    Result.Save           := List.Values['Save']           ;
    Result.Monarch        := List.Values['Monarch']        ;
    Result.CashOnDelivery := List.Values['CashOnDelivery'] ;
    Result.Arc_Fregat     := List.Values['Arc_Fregat']     ;
    Result.FFiles         := List.Values['FFiles']         ;
  finally
    FreeAndNil(List);
  end;
end;

end.
