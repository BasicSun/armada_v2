unit uTS_ImportGrade;

interface
uses
  uvstDIP_Types, uPathes,IBDatabase, uRSK_Code, Types;

procedure LoadCSV_ImportGrade(DB: TIBDatabase; const rPathes:TrPathes; const
    Course_ID: Integer; const FCourse_Code:uRSK_Code.TRSK_Code; const
    date_from: TDateTime; out aStudent_ID: TIntegerDynArray; out AMessage:
    TStringDynArray; var Last_Work_Grade_D: TDateTime);

implementation
uses
  SysUtils, raCSV, uParams, IBLib, uCommunicationHTTP, ZUM_Const,
  raCodePage, IBSQL, uRSK_StudentWork, NumJob, strJob, CustomInsUpd;

procedure LoadCSV_ImportGrade(DB: TIBDatabase; const rPathes:TrPathes; const
    Course_ID: Integer; const FCourse_Code:uRSK_Code.TRSK_Code; const
    date_from: TDateTime; out aStudent_ID: TIntegerDynArray; out AMessage:
    TStringDynArray; var Last_Work_Grade_D: TDateTime);
type
  TImportDiscipline = record
    Discipline_nm: string;
    Discipline_ID: Integer;
    COURSE_DISCIPLINE_ID: Integer;
  end;
  TImportGrade = record
    Student_Uchmet_Id, Student_Id:Integer;
    Max_ball: Integer;
    SGrade: string;
    GRADE_VALUE_ID: Integer;
    Grade_D:TDateTime;
    COURSE_DISCIPLINE_ID: Integer;
  end;
  procedure Save_Last_Work_Grade_D(TRW: TIBTransaction);
  var
    iu: TInsUpd;

  begin
    Last_Work_Grade_D := GetValueField(TRW,
          'Select max(WORK_DATE) From DIP_STUDENT_WORK Where COURSE_ID=:COURSE_ID',
          ['COURSE_ID'],[Course_ID]);
    iu := TInsUpd.Create(nil, TRW, kiuUpdate, 'DIP_COURSE');
    try
      iu.AddWhere('COURSE_ID',COURSE_ID);
      iu.ValueDate('Last_Work_Grade_D', Last_Work_Grade_D, True);
      iu.ExecSQL;
    finally
      iu.Free;
    end;
  end;
var
  csv: TraCSV2;
  FFileNameCSV: string;
  I: Integer;
  Param: TParams;
  RSK_Code: TRSK_Code;
  S: string;
  TRR: TLibTransaction;
  URL: string;
  Value:TStringDynArray;
  AID: array of TImportDiscipline;
  iID: Integer;
  AIG: array of TImportGrade;
  iIG: Integer;

  q: TIBSQl;
  RSK_StudentWork: TRSK_StudentWork;
  TRW: TLibTransaction;

begin
  // TODO -cMM: LoadCSV_ImportGrade default body inserted
//  URL := 'https://www.uchmet.ru/admin/rsk/exam_results/export_results.php?course_code={���_�����}&date_from=01.04.2019';
  URL := 'https://www.uchmet.ru/admin/rsk/exam_results/export_results.php?';
//          https://www.uchmet.ru/admin/rsk/exam_results/export_results.php?course_code=�����-18&date_from=21.03.2020
//  URL := format(URL,[lbCourse.Caption, DateToStr(dtFrom.Date)]);



  Param := TParams.Create;
  try
    Param.SetValue('course_code', FCourse_Code.CodeURL); //EncodeURLElement(
    Param.SetValueDate('date_from', date_from);
    URL := URL + Param.Text_asURL
  finally
    Param.Free;
  end;
  S := GetInetToString(URL).DataString;
  if s <> 'OK' then
    begin
      AddStrToArray(AMessage,Format('����� %s',[s]));
      Exit;
    end;

  URL := format('https://%swww.uchmet.ru/upload/uchmet.ru/rsk/export/exam/%s.csv',
    [uchmet_pass,FCourse_Code.CodeURL]);
  FFileNameCSV := rPathes.Save + 'ImportGrade.csv';
  if FileExists(FFileNameCSV) then
    DeleteFile(FFileNameCSV);
  HTTP_FileLoad(URL, FFileNameCSV, 300000);
  if not FileExists(FFileNameCSV) then
    begin
      AddStrToArray(AMessage,'�� ���������� ���� � �����.��');
      Exit;
    end;
  TRR := Create_TRR(DB, True);
  try
    csv := TraCSV2.Create;
    try
      csv.CodePage := cpUTF8;
      //������� �������� �����, �.�. csv �� ������
      csv.ControlCountValues := False;

      csv.LoadFromFile(FFileNameCSV,3);

      for I := 0 to csv.Count - 1 do
        begin
          Value := csv.Values[I];

          //��� �����
          if (I=0) and (Length(Value)=1) then
            begin
              RSK_Code.Code := Value[0];
              if (FCourse_Code.Code = RSK_Code.Code) then
                Continue
              else
              begin
                AddStrToArray(AMessage,'��� ����� ����������');
//                      ShowMessageFmt('������ ������������ ����� %s �� ������������ �������� ����� %s',
//                        [Value[0], frCourse.Focused_Course]);
                Exit;
              end;
            end;

          //��� ������ � ������ ������ �������
          if (csv.Count_Fields=Length(Value)) and
             (Value[0]=csv.Fields[0]) then
            Continue;

          //����������
          if (I>0) and (Length(Value)=1) then
          begin
            iID := Length(AID);
            SetLength(AID, iID+1);
            AID[iID].Discipline_nm := Value[0];

            q := TIBSQl.Create(nil);
            try
              q.Transaction := TRR;
              q.SQL.Text := 'Select * from SPV_DIP_WORK_GRADE(:COURSE_ID, 0, 0)';
              q.SQL.Add('where upper(Discipline_nm)=upper(:Discipline_nm)');
              q.ParamByName('COURSE_ID').AsInteger := COURSE_ID;
              q.ParamByName('Discipline_nm').AsString := AID[iID].Discipline_nm;
              q.ExecQuery;
              if q.RecordCount > 0 then
                begin
                  AID[iID].Discipline_ID := q.FieldByName('Discipline_ID').AsInteger;
                  AID[iID].COURSE_DISCIPLINE_ID:= q.FieldByName('COURSE_DISCIPLINE_ID').AsInteger;
                end;
              q.Close;
              if AID[iID].COURSE_DISCIPLINE_ID = 0 then
                begin
                  q.SQL.Text := 'Select * from SPV_DIP_WORK_GRADE(:COURSE_ID, 0, 0) sdwg';
                  q.SQL.Add('join DIP_COURSE_DISCIPLINE_IMPORT dcdi ON dcdi.DISCIPLINE_ID=sdwg.DISCIPLINE_ID');
                  q.SQL.Add('where (dcdi.DISCIPLINE_IMPORT)=upper(:Discipline_nm)');
                  q.ParamByName('COURSE_ID').AsInteger := COURSE_ID;
                  q.ParamByName('Discipline_nm').AsString := AID[iID].Discipline_nm;
                  q.ExecQuery;
                  if q.RecordCount > 0 then
                    begin
                      AID[iID].Discipline_ID := q.FieldByName('Discipline_ID').AsInteger;
                      AID[iID].COURSE_DISCIPLINE_ID:= q.FieldByName('COURSE_DISCIPLINE_ID').AsInteger;
                    end;
                end;
            finally
              q.Free;
            end;
            if AID[iID].COURSE_DISCIPLINE_ID = 0 then
              begin
                AddStrToArray(AMessage,
                  Format('�� ���� ���������������� ���������� "%s',
                         [AID[iID].Discipline_nm]));
                Exit;
              end;

          end;

          //������
          if (csv.Count_Fields = Length(Value)) then
          begin
            iIG := Length(AIG);
            SetLength(AIG, iIG+1);
            AIG[iIG].Student_Uchmet_Id := StrToInt(Value[0]);
            AIG[iIG].Max_ball:= StrToInt(Value[1]);
            AIG[iIG].SGrade:= Value[2];

            AIG[iIG].GRADE_VALUE_ID := GetValueFieldInt(TRR,
              'Select GRADE_VALUE_ID From DIP_GRADE_VALUE Where GRADE_VALUE_NM=:GRADE_VALUE_NM',
              ['GRADE_VALUE_NM'],[AIG[iIG].SGrade]);
            if AIG[iIG].GRADE_VALUE_ID> 0 then
            else
            if AIG[iIG].SGrade = '�������������������' then
              AIG[iIG].GRADE_VALUE_ID := 2
            else
            if AIG[iIG].SGrade = '�����������������' then
              AIG[iIG].GRADE_VALUE_ID := 3
            else
            if AIG[iIG].SGrade = '������' then
              AIG[iIG].GRADE_VALUE_ID := 4
            else
            if AIG[iIG].SGrade = '�������' then
              AIG[iIG].GRADE_VALUE_ID := 5
            else
            if AIG[iIG].SGrade = '�� �������' then
              AIG[iIG].GRADE_VALUE_ID := 6
            else
            if AIG[iIG].SGrade = '�����' then
              AIG[iIG].GRADE_VALUE_ID := 7
            else
              raise Exception.CreateFmt('�� ��������� ������ %s',[AIG[iIG].SGrade]);

            AIG[iIG].Grade_D:=StrToDateTime(Value[3]);
            AIG[iIG].COURSE_DISCIPLINE_ID := AID[iID].COURSE_DISCIPLINE_ID;

            AIG[iIG].Student_Id := GetValueFieldInt(TRR,
              'Select Student_id from dip_student where STUDENT_UCHMET_ID=:STUDENT_UCHMET_ID',
              ['STUDENT_UCHMET_ID'],
              [AIG[iIG].Student_Uchmet_Id]);
            if AIG[iIG].Student_Id = 0 then
              begin
                AddStrToArray(AMessage,
                  Format('�� ������ ��������� � ID = %d',
                    [AIG[iIG].Student_Uchmet_Id]));
                Exit;
              end;
          end;

        end;

    finally
      csv.Free;
    end;


    TRW := Create_TRW(TRR);
    try
      for iIG := 0 to High(AIG) do
      begin
        if AIG[iIG].Student_Id = 0 then
          Continue;
        RSK_StudentWork := TRSK_StudentWork.Create(TRR, COURSE_ID);
        try
          RSK_StudentWork.Reload(AIG[iIG].COURSE_DISCIPLINE_ID, AIG[iIG].Student_Id);

          RSK_StudentWork.RATING := AIG[iIG].Max_ball;
          RSK_StudentWork.GRADE_VALUE_ID := AIG[iIG].GRADE_VALUE_ID;
          RSK_StudentWork.WORK_DATE := AIG[iIG].Grade_D;
          if RSK_StudentWork.Modify then
            begin
              RSK_StudentWork.Save(TRW);
              if not ContainsItem(aStudent_ID, AIG[iIG].Student_Id) then
                AddIntToArray(aStudent_ID, AIG[iIG].Student_Id);
            end;

        finally
          RSK_StudentWork.Free;
        end;
      end;
      Save_Last_Work_Grade_D(TRW);


      if TRW.InTransaction then
        TRW.Commit;
    finally
      TRW.Rollback_and_Free;
    end;
  finally
    TRR.Commit_and_Free;
  end;

end;

end.
