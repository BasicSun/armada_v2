program Armada_v2;

uses
  Vcl.Forms,
  f_Main in '..\Form\f_Main.pas' {fMain},
  f_DMGUI in '..\Form_GUI\f_DMGUI.pas' {dmGUI: TDataModule},
  uPathes in '..\Unit_Fregat\uPathes.pas',
  uRSK_Code in '..\Unit_RSK\uRSK_Code.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmGUI, dmGUI);
  Application.CreateForm(TfMain, fMain);
  Application.Run;
end.
