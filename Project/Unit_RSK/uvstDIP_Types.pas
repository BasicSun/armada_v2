unit uvstDIP_Types;

interface

uses
  Types, Graphics, IBDatabase, IBSQL,
  uEnvelope, uUM_Address, uFIO, uTS_CalcNode, SysUtils, ZUM_Const,
  uDIP_Types, VirtualTrees, uIdentifiers_Parcels, uRSK_Types;

const
//  ACourse_type_Code : array[1..5] of string =
//    ('���', '���', '���', '���', '���');
  ACourse_type_nm : array[1..4] of string =
    ('������ � ���������������� ��������������',
     '������ � ���������� ������������',
     '������������� � ��������� ������������',
     '������������� � ���������');


type


  TRSK_LoadingMode = (rlmNone, rlmRSK, rlmDiplom);

  TRSK_Code = record
    Course_type  : string;
    Course_cd    : string;
    SubCourse_cd : string;
    Group_cd     : string;
    FGroup_Number : string;
    IsCorrect    : Boolean;
    IsActive     : Boolean;
  private
    FCode        : string;
    FPosGroup    : Integer;
    FCourse_Type_Id : Integer;
    FCourse_ID    : Integer;
    FGroup_Id     : Integer;
    FCourse_Number: string;
    FCourse_Offline: string;
    function GetCode: string;
    procedure SetCode(const Value: string);
    function GetIsGroupInCourse: Boolean;
    procedure SetCourse_Type_Id(const Value: Integer);
    procedure SetCourse_ID(const Value: Integer);
    function GetCourseIsGroup: Boolean;
    procedure SetGroup_ID(const Value: Integer);
    function GetIsSubCourse: Boolean;
    function GetCodeU: string;
    procedure Build;
    procedure SetGroup_Number(const Value: string);
    procedure SetCourse_Number(const Value: string);
    procedure SetCourse_Offline(const Value: string);
    function GetCodeC: string;
    procedure SetCodeC(const Value: string);
    function GetCodeURL: AnsiString;
    function GetIsFooter: Boolean;
    function PosGroupInCourse(const Group_code: string): Integer;
    function GetAutoCourse_type_ID: Integer;
  public
    procedure Clear;
    procedure Make_Course_ID(TRW: TIBTransaction);
    function GetCourseFromCode(const Group_code: string): string;
    property Code: string read GetCode write SetCode;
    property CodeU: string read GetCodeU;
    property CodeC: string read GetCodeC write SetCodeC;
    property CodeURL: AnsiString read GetCodeURL;
    property IsGroupInCourse: Boolean read GetIsGroupInCourse;
    property IsSubCourse: Boolean read GetIsSubCourse;
    property IsFooter: Boolean read GetIsFooter;
    property CourseIsGroup: Boolean read GetCourseIsGroup;
    property Course_Type_Id: Integer read FCourse_Type_Id write SetCourse_Type_Id;
    property Course_ID: Integer read FCourse_ID write SetCourse_ID;
    property Group_ID: Integer read FGroup_ID write SetGroup_ID;
    property Group_Number : string read FGroup_Number write SetGroup_Number;
    property Course_Number: string read FCourse_Number write SetCourse_Number;
    property Course_Offline: string read FCourse_Offline write SetCourse_Offline;
    property AutoCourse_type_ID: Integer read GetAutoCourse_type_ID;
  end deprecated 'uRSK_Code.TRSK_Code';

  TOrganization = class
  private
    FORGANIZATION_ID: Integer;
  private
    procedure SetORGANIZATION_ID(const Value: Integer);
  public
    property ORGANIZATION_ID: Integer read FORGANIZATION_ID write SetORGANIZATION_ID;
  end;

  TCourse_Type = class
  private
    FCourse_Type_ID: Integer;
    procedure SetCourse_Type_ID(const Value: Integer);
  public
    destructor Destroy; override;
    property Course_Type_ID: Integer read FCourse_Type_ID write SetCourse_Type_ID;
  end;

  TStatusGroup = record
    Open_State_Id: Integer;
//    Documents: Integer;
//    Akadem : Integer;
//    FinanceDolg: Boolean;
    //ToRPD: Boolean;
  end;

  TStatus_Duty_Akadem = (sdaNone = 0, sdaRed = 1, sdaYellow = 2);

  TCourse_Group = class
  private
    FDECREE_OF_DISMISSAL_ID: Integer;
    FDECREE_OF_ENROLLMENT_ID: Integer;
    FD_LEARN_END: TDateTime;
    FD_LEARN_START: TDateTime;
    FORDER_OF_DISMISSAL: string;
    FORDER_OF_ENROLLMENT: string;
    FProfession_nm: string;
    //FProfession_ID: Integer;
    FIn_Volume_Hours: Integer;
    FModify: Boolean;
    FSIGNATURE_ID: Integer;
    //FORGANIZATION_ID: Integer;
    FBlank_type: Integer;
    FSCAN_PATH_ID: Integer;
    FSCAN_PATH_NM: string;
    FD_ISSUE: TDateTime;

//    FOpen_State_Id: Integer;
    FStatusGroup: TStatusGroup;
    FDuty_Finance: Boolean;
    FDuty_Akadem: TStatus_Duty_Akadem;
    FDuty_Doc: Boolean;
    FNoDuty_to_Print: Boolean;
    FDuty_Change_d: TDateTime;
    FIssue_D_Max: TDateTime;
    FIssue_D_Min: TDateTime;
    FIs_Archiv_LD: Boolean;
    FSIGNATURE_KOMISSION_ID: Integer;
    FPrint_need_Blank: Boolean;
    FLast_Import_Statement_D: TDateTime;
    FObrazec_not_Confirmed: Boolean;
    FTransferred_to_UchMag: Boolean;
    FSend_to_FRDO: Boolean;
    FPath_Archiv_NM: string;
    FPath_Archiv_ID: Integer;
    function Getis_Learn_Dates: Boolean;
    function GetSD_LEARN_END: string;
    function GetSD_LEARN_START: string;
    procedure SetDECREE_OF_DISMISSAL_ID(const Value: Integer);
    procedure SetDECREE_OF_ENROLLMENT_ID(const Value: Integer);
    procedure SetD_LEARN_END(const Value: TDateTime);
    procedure SetD_LEARN_START(const Value: TDateTime);
    procedure SetORDER_OF_DISMISSAL(const Value: string);
    procedure SetORDER_OF_ENROLLMENT(const Value: string);
    //procedure SetProfession_ID(const Value: Integer);
    procedure SetProfession_nm(const Value: string);
    procedure SetIn_Volume_Hours(const Value: Integer);
    procedure SetModify(const Value: Boolean);
    procedure SetSIGNATURE_ID(const Value: Integer);
    //procedure SetORGANIZATION_ID(const Value: Integer);
    procedure SetBlank_type(const Value: Integer);
    procedure SetSCAN_PATH_ID(const Value: Integer);
    procedure SetSCAN_PATH_NM(const Value: string);
    procedure SetD_ISSUE(const Value: TDateTime);
    function GetSD_ISSUE: string;
    procedure SetOpen_State_Id(const Value: Integer);
    procedure SetStatusGroup(const Value: TStatusGroup);
    function GetOpen_State_Id: Integer;
    procedure SetDuty_Akadem(const Value: TStatus_Duty_Akadem);
    procedure SetDuty_Doc(const Value: Boolean);
    procedure SetDuty_Finance(const Value: Boolean);
    procedure SetNoDuty_to_Print(const Value: Boolean);
    procedure SetDuty_Change_d(const Value: TDateTime);
    procedure SetIssue_D_Max(const Value: TDateTime);
    procedure SetIssue_D_Min(const Value: TDateTime);
    procedure SetIs_Archiv_LD(const Value: Boolean);
    procedure SetSIGNATURE_KOMISSION_ID(const Value: Integer);
    procedure SetPrint_need_Blank(const Value: Boolean);
    procedure SetLast_Import_Statement_D(const Value: TDateTime);
    procedure SetObrazec_not_Confirmed(const Value: Boolean);
    procedure SetTransferred_to_UchMag(const Value: Boolean);
    procedure SetSend_to_FRDO(const Value: Boolean);
    procedure SetPath_Archiv_ID(const Value: Integer);
    procedure SetPath_Archiv_NM(const Value: string);
  public
    Decree_of_Enrollment, Decree_of_Dismissal: PDecree;
    constructor Create;
    procedure Save();
    property Modify: Boolean read FModify write SetModify;
    /// <summary>
    /// ���� ������ ��������
    /// </summary>
    property D_LEARN_START: TDateTime read FD_LEARN_START write SetD_LEARN_START;
    property SD_LEARN_START: string read GetSD_LEARN_START;
    /// <summary>
    /// ���� ��������� ��������
    /// </summary>
    property D_LEARN_END: TDateTime read FD_LEARN_END write SetD_LEARN_END;
    property SD_LEARN_END: string read GetSD_LEARN_END;
    property D_ISSUE: TDateTime read FD_ISSUE write SetD_ISSUE;
    property SD_ISSUE: string read GetSD_ISSUE;
    /// <summary>
    /// ������ � ����������
    /// </summary>
    property ORDER_OF_ENROLLMENT: string read FORDER_OF_ENROLLMENT write SetORDER_OF_ENROLLMENT;
    /// <summary>
    /// ������ �� ����������
    /// </summary>
    property ORDER_OF_DISMISSAL: string read FORDER_OF_DISMISSAL write SetORDER_OF_DISMISSAL;

    property DECREE_OF_ENROLLMENT_ID: Integer read FDECREE_OF_ENROLLMENT_ID write SetDECREE_OF_ENROLLMENT_ID;
    property DECREE_OF_DISMISSAL_ID: Integer read FDECREE_OF_DISMISSAL_ID write SetDECREE_OF_DISMISSAL_ID;

    // ���������������� ���������
    //property Profession_ID: Integer read FProfession_ID write SetProfession_ID;
    property Profession_nm: string read FProfession_nm write SetProfession_nm;
    property In_Volume_Hours: Integer read FIn_Volume_Hours write SetIn_Volume_Hours;
    property SIGNATURE_ID: Integer read FSIGNATURE_ID write SetSIGNATURE_ID;
    property SIGNATURE_KOMISSION_ID: Integer read FSIGNATURE_KOMISSION_ID write SetSIGNATURE_KOMISSION_ID;
    //property ORGANIZATION_ID1: Integer read FORGANIZATION_ID write SetORGANIZATION_ID;
    //��� ������ �� ����
    property Blank_type: Integer read FBlank_type write SetBlank_type;
    property SCAN_PATH_ID : Integer read FSCAN_PATH_ID write SetSCAN_PATH_ID;
    property SCAN_PATH_NM : string read FSCAN_PATH_NM write SetSCAN_PATH_NM;

    property Path_Archiv_ID: Integer read FPath_Archiv_ID write SetPath_Archiv_ID;
    property Path_Archiv_NM: string read FPath_Archiv_NM write SetPath_Archiv_NM;


    property Open_State_Id: Integer read GetOpen_State_Id write SetOpen_State_Id;
    property StatusGroup: TStatusGroup read FStatusGroup write SetStatusGroup;
    property Duty_Doc: Boolean read FDuty_Doc write SetDuty_Doc;
    property Duty_Akadem : TStatus_Duty_Akadem read FDuty_Akadem write SetDuty_Akadem;
    property Duty_Finance : Boolean read FDuty_Finance write SetDuty_Finance;
    /// <summary>
    /// ������ ���, ��������� � ������
    /// </summary>
    property NoDuty_to_Print : Boolean read FNoDuty_to_Print write SetNoDuty_to_Print;
    property Print_need_Blank: Boolean read FPrint_need_Blank write SetPrint_need_Blank;
    property Obrazec_not_Confirmed: Boolean read FObrazec_not_Confirmed write SetObrazec_not_Confirmed;
    property Duty_Change_d: TDateTime read FDuty_Change_d write SetDuty_Change_d;
    property Last_Import_Statement_D: TDateTime read FLast_Import_Statement_D write SetLast_Import_Statement_D;
    property Transferred_to_UchMag: Boolean  read FTransferred_to_UchMag write SetTransferred_to_UchMag;
    property Is_Learn_Dates: Boolean read Getis_Learn_Dates;
    property Send_to_FRDO: Boolean read FSend_to_FRDO write SetSend_to_FRDO;
    property Is_Archiv_LD: Boolean read FIs_Archiv_LD write SetIs_Archiv_LD;
    property Issue_D_Min: TDateTime read FIssue_D_Min write SetIssue_D_Min;
    property Issue_D_Max: TDateTime read FIssue_D_Max write SetIssue_D_Max;
  end;

  PvstProgramm = ^TvstProgramm;
  TvstProgramm = record
    COURSE_PROGRAMM_ID    : Integer;
    COURSE_PROGRAMM_NM    : String;
    COURSE_PROGRAMM_PRINT : string;
  end;

  //��������� ���������� �� �����.
  //1.������ �������
  //2.�������� � �������� ������
  //3.���
  TCourseStudentVisible = (csvNewAndOpenClose, csvOnlyNew, csvOpenClose, csvAll);

  PDIPCourse = ^TDIPCourse;

  TDIPCourse = class
  private
//    FCOURSE_PROGRAMM_NM: string;
//    FCOURSE_PROGRAMM_ID: Integer;
    FSPHERE_OF_ACTIVITY_NM: string;
    FSPHERE_OF_ACTIVITY_ID: Integer;
    FREG_NUMBER_TEMPL_ID: Integer;
    FQUALIFICATION_ID: Integer;
    FRegNumberPrefix: string;
    FRegNumberPostfix: string;
    FIN_VOLUME_HOURS: Integer;
    FIs_Practice: Boolean;
    FLast_Import_Statement_D: Variant;
    FLast_Uchmet_Statement_D: Variant;
    FStudents_Outside_the_group: Boolean;
    FModify: Boolean;
    FORGANIZATION_ID: Integer;
    FCOURSE_PROGRAMM: TvstProgramm;
    FLast_Work_Grade_D: Variant;
    FLast_Uchmet_Grade_D: TDateTime;
    FForm_of_Implementation: Integer;
    FType_of_Instruction: Integer;
//    procedure SetCOURSE_PROGRAMM_ID(const Value: Integer);
//    procedure SetCOURSE_PROGRAMM_NM(const Value: string);
    procedure SetSPHERE_OF_ACTIVITY_ID(const Value: Integer);
    procedure SetSPHERE_OF_ACTIVITY_NM(const Value: string);
    procedure SetREG_NUMBER_TEMPL_ID(const Value: Integer);
    procedure SetQUALIFICATION_ID(const Value: Integer);
    procedure SetRegNumberPrefix(const Value: string);
    procedure SetRegNumberPostfix(const Value: string);
    procedure SetIN_VOLUME_HOURS(const Value: Integer);
    procedure SetIs_Practice(const Value: Boolean);
    function GetPos_ContainsFGOS: Integer;
    function GetIs_ContainsFGOS: Boolean;
    function GetIs_ContainsSubject: Boolean;
    function GetPos_ContainsSubject: Integer;
    function GetIs_ContainsSpecialization: Boolean;
    function GetPos_ContainsSpecialization: Integer;
    function GetIs_ContainsProgramm_Obraz: Boolean;
    function GetPos_ContainsProgramm_Obraz: Integer;
    procedure SetLast_Import_Statement_D(const Value: Variant);
    procedure SetStudents_Outside_the_group(const Value: Boolean);
    procedure SetModify(const Value: Boolean);
    procedure SetLast_Uchmet_Statement_D(const Value: Variant);
    procedure SetORGANIZATION_ID(const Value: Integer);
    procedure SetCOURSE_PROGRAMM(const Value: TvstProgramm);
    function GetCOURSE_PROGRAMM_NM: string;
    procedure SetCOURSE_PROGRAMM_NM(const Value: string);
    function GetCOURSE_PROGRAMM_ID: Integer;
    function GetLast_Grade_Min: TDateTime;
    procedure SetCOURSE_PROGRAMM_ID(const Value: Integer);
    function GetNeed_Import_Statement: Boolean;
    procedure SetLast_Work_Grade_D(const Value: Variant);
    procedure SetLast_Uchmet_Grade_D(const Value: TDateTime);
    function GetNeed_Import_Grade: Boolean;
    function GetType_of_Instruction_Caption: string;
    procedure SetForm_of_Implementation(const Value: Integer);
    procedure SetType_of_Instruction(const Value: Integer);
  public
    //Course_Type_ID1: Integer;
    //Course_Type_nm: string;
    /// <summary>��� ������ �� ����</summary>
  //  Blank_type: Integer;

    COUNT_DISCIPLINE      : Integer;
    COUNT_FINAL_WORK_TYPE : Integer;
    COUNT_DISCIPLINE_ARCHIV: Integer;
    CourseDisciplines: TFCourseDisciplines;
    /// <summary>
    /// ��������� ���������� �� �����    ///
    /// </summary>
    CourseStudentVisible: TCourseStudentVisible;
    constructor Create;
    destructor Destroy; override;
    procedure CourseProgramm_Save(TRW: TIBTransaction);
//    property COURSE_PROGRAMM_ID: Integer read FCOURSE_PROGRAMM_ID write SetCOURSE_PROGRAMM_ID;
//    property COURSE_PROGRAMM_NM: string read FCOURSE_PROGRAMM_NM write SetCOURSE_PROGRAMM_NM;
    function ProgrammMacros(const AFGOS,ASubject,ASpecialization,
      AProgramm_obraz,AProgramm_obraz_red: string): string;
    property Modify: Boolean read FModify write SetModify;
    property Last_Import_Statement_D: Variant read FLast_Import_Statement_D write SetLast_Import_Statement_D;
    property Last_Uchmet_Statement_D: Variant read FLast_Uchmet_Statement_D write SetLast_Uchmet_Statement_D;
    /// <summary>
    /// ����� ��������� ������ �� ����� ���������� � ����. ����� ������������
    /// � �������� � ������ ��� ��������� ����� ������
    /// </summary>
    property Last_Work_Grade_D: Variant read FLast_Work_Grade_D write SetLast_Work_Grade_D;
    property Last_Uchmet_Grade_D: TDateTime read FLast_Uchmet_Grade_D write SetLast_Uchmet_Grade_D;
    property Last_Grade_Min: TDateTime read GetLast_Grade_Min;
    property Need_Import_Statement: Boolean read GetNeed_Import_Statement;
    property Need_Import_Grade: Boolean read GetNeed_Import_Grade;
    property SPHERE_OF_ACTIVITY_ID: Integer read FSPHERE_OF_ACTIVITY_ID write SetSPHERE_OF_ACTIVITY_ID;
    property SPHERE_OF_ACTIVITY_NM: string read FSPHERE_OF_ACTIVITY_NM write SetSPHERE_OF_ACTIVITY_NM;
    property REG_NUMBER_TEMPL_ID: Integer read FREG_NUMBER_TEMPL_ID write SetREG_NUMBER_TEMPL_ID;
    property QUALIFICATION_ID: Integer read FQUALIFICATION_ID write SetQUALIFICATION_ID;
    property RegNumberPrefix: string read FRegNumberPrefix write SetRegNumberPrefix;
    property RegNumberPostfix: string read FRegNumberPostfix write SetRegNumberPostfix;
    property IN_VOLUME_HOURS: Integer read FIN_VOLUME_HOURS write SetIN_VOLUME_HOURS;
    property Is_Practice: Boolean read FIs_Practice write SetIs_Practice;
    property Pos_ContainsFGOS: Integer read GetPos_ContainsFGOS;
    property Is_ContainsFGOS: Boolean read GetIs_ContainsFGOS;
    property Pos_ContainsSubject: Integer read GetPos_ContainsSubject;
    property Is_ContainsSubject: Boolean read GetIs_ContainsSubject;
    property Pos_ContainsSpecialization: Integer read GetPos_ContainsSpecialization;
    property Is_ContainsSpecialization: Boolean read GetIs_ContainsSpecialization;
    property Pos_ContainsProgramm_Obraz: Integer read GetPos_ContainsProgramm_Obraz;
    property Is_ContainsProgramm_Obraz: Boolean read GetIs_ContainsProgramm_Obraz;
    property Students_Outside_the_group: Boolean read FStudents_Outside_the_group write SetStudents_Outside_the_group;
    property ORGANIZATION_ID: Integer read FORGANIZATION_ID write SetORGANIZATION_ID;
    property COURSE_PROGRAMM: TvstProgramm read FCOURSE_PROGRAMM write SetCOURSE_PROGRAMM;
    property COURSE_PROGRAMM_NM: string read GetCOURSE_PROGRAMM_NM write SetCOURSE_PROGRAMM_NM;
    property COURSE_PROGRAMM_ID: Integer read GetCOURSE_PROGRAMM_ID write SetCOURSE_PROGRAMM_ID;
    property Form_of_Implementation: Integer read FForm_of_Implementation write SetForm_of_Implementation;
    property Type_of_Instruction: Integer read FType_of_Instruction write SetType_of_Instruction;
    property Type_of_Instruction_Caption: string read
        GetType_of_Instruction_Caption;
  end;

  TParamCourse = record
    Loading: Boolean;
    //cntDoc: Integer;
  end;


  PvstCourse = ^TvstCourse;

  TvstCourse = record
    RSK_Code: TRSK_Code;
    //�����������
    Organization: TOrganization;

    // ������ �� ���� �����
    //Course_Type3 : TCourse_Type;
   // Course_type_active: Boolean;
    // ����
    Course      :TDIPCourse;
    Course_active: Boolean;
    // ������ �� ������
    Course_Group: TCourse_Group;

    SName2: string;
    cnt_RUF: Integer;
    cnt_Doc: Integer;
    Max_D_Vydan: TDateTime;
    ImportStatement_Last_D: Variant;
    GroupFilter: TIntegerDynArray;
    ORGANIZATION_NM: string;
    Param: array[TRSK_LoadingMode] of TParamCourse;


    //Modify: Boolean;
  private
    function GetWrongOption: Boolean;
    function Getis_Learn_Dates: Boolean;
  public
    procedure Assign(const Source: TvstCourse);
    property WrongOption: Boolean read GetWrongOption;
    property is_Learn_Dates: Boolean read Getis_Learn_Dates;
  end;

  ///<summary>
  ///�����/����� ���������
  ///</summary>
  TDocument_SN = record
  private
    FNumberFull: string;
    procedure SetNumberFull(const Value: string);
    function GetSeria: string;
    function GetNumber: string;
    function DelimiterIndex: Integer;
  public
    property NumberFull: string read FNumberFull write SetNumberFull;
    property Seria: string read GetSeria;
    property Number: string read GetNumber;
  end;



  TDIP_Pochta = record
    //���������� � �������� �����
    D_ODO_SENDS: TDateTime;
    // ���� �������
    D_Reestr: TDateTime;
    // ���������� �� �����
    D_SEND_TO_POST: TDateTime;
  end;

  TDiscipline = record
    Discipline_ID: Integer;
    {$IF RSK_DISCIPLINE_PARENT=1}
    Discipline_Parent_ID: Integer;
    {$IFEND}
    Discipline_nm: string;
    Discipline_nm2: string;
    CNT_COURSE : Integer;

    Final_Work_Type: Boolean;
    Final_Work_Theme: Boolean;
  end;

  PvstCourse_Discipline = ^TvstCourse_Discipline;

  TvstCourse_Discipline = record
    Dicipline_Order: Integer;
    COURSE_DISCIPLINE_ID: Integer;
    Discipline: TDiscipline;
    {$IF RSK_DISCIPLINE=1}
    Discipline_ID_Old: Integer;
    {$IFEND}
    Capacity_ID: Integer;
    Capacity_nm: string;
    CAPACITY_HOURS: Integer;
    Archiv: Boolean;

    Modify: Boolean;
  end;

  PvstDDiscipl = ^TvstDDiscipl;
  TvstDDiscipl = record
    Active      : Boolean;
    Course: TvstCourse_Discipline;
    CERTIFICATE_GRADE_ID : Integer;
    Discipline: TDiscipline;

    Capacity_Fregat_ID: Integer;
    GradeVal_nm : string;

    DISCIPLINE_ORDER: Integer;

    //��� �������
    GRADE_VALUE_ID: Integer;
    IS_PERMISSION : Boolean;
  end;

  // ������ ���������
  TADDiscipl = array of TvstDDiscipl;

  TDIP_ZAKAZ_INFO=record
    id      : Integer;
    id_Order: Integer;
    id_Zak  : Integer;
    FIO_ID  : Integer;
    Phone   : TStringDynArray;
    eMail   : TStringDynArray;

    Address : TUM_Address;
//      Pochta:array of TDIP_Pochta;
    Pochta: TDIP_Pochta;
    PACK_RPO_ID: Integer;
    IDC_POSTALITEM: string;
    Surnames_RPO: string;
    Contact_ID: Integer;
  end;

  PvstDiplom = ^TvstCertificateInfo;

  PvstCertificateInfo = ^TvstCertificateInfo;
  TvstCertificateInfo = record
    //Diplom_ID: Integer;

    FIO_ID: Integer;
    FIO: TFio;
    RSK_Code: TRSK_Code;

    Certificate: TDip_Certificate;
    //Envelope: TEnvelope;
    Basic: record
      ID: Integer;
      DT: TDateTime;
      Number: string;
    end;
    Pred_Obr_nm: string;
    // ���������� ����������
    Pril: Byte;
    // ���������� pdf ������
    pdf_Count: Integer;
    // ��������� �� ����������� ����
    Calc : TCalcNode;
    DIP_ZAKAZ_INFO: TDIP_ZAKAZ_INFO;
    /// �������� ����������
    //pdf_Printed: Boolean;
    /// ���������� �������
//    Sent_To_Client: Boolean;
    // ������ ���������
    ADDiscipl: TADDiscipl;
    //������������ ������ ������
    NodeParent_Group: PVirtualNode;
    RSK : record
      DogovorsNo: string;
      Is_LightWeight: Boolean;
      Not_Transfer_to_Archive: Boolean;
    end;
  public
    procedure Clear;
    procedure CopyFrom(const ADataCI: TvstCertificateInfo);
    procedure PrepareForDublikat;
  end;

  TOnSavePrinted_Dt = procedure(var DataCI: TvstCertificateInfo) of object;

  TObrazec = record
    Template: string;
    Pref: string;
    Fon: TStringDynArray;
    Nabor6: Boolean;
    BackPicture: Boolean;
    dip_Discipline: Boolean;
    dip_Discipl_6_row: Boolean;
    Hours_Total: Boolean;
    OnlyJpeg: Boolean;
    CountJpeg: Integer;
    FocusJpeg: Boolean;
    Version: Integer;
    Blank_ID: Integer;
  end;

  TEducation = record
    EDUCATION_LEVEL_ID  : Integer;
    EDUCATION_DIPLOM_NO : TDocument_SN;
    EDUCATION_DIPLOM_D  : TDateTime;
  end;

  PvstEducation = ^TvstEducation;

  TvstEducation = record
    EDUCATION_LEVEL_ID   : Integer;
    EDUCATION_LEVEL_NAME : string;
    EDUCATION_LEVEL_PRINT: string;
    VARIANTS_LOAD        : string;
    VARIANTS_LOAD_ORDER  : Integer;
  end;

  PvstSpecialization = ^TvstSpecialization;

  TvstSpecialization = record
    Specialization_ID: Integer;
    Specialization_nm: string;
  end;

  PvstQualification = ^TvstQualification;

  TvstQualification = record
    ID: Integer;
    Qualification_id: Integer;
    Qualification_nm: string;
//    Qualification_nm_original: string;
    Hide  : Boolean;
    Count : Integer;
    FQual:TFQualification;
  end;

  PvstPROGRAMM_OBRAZ = ^TvstPROGRAMM_OBRAZ;

  TvstPROGRAMM_OBRAZ = record
    PROGRAMM_OBRAZ_ID  : Integer;
    PROGRAMM_OBRAZ_NM  : string;
    PROGRAMM_OBRAZ_RED : string;
    PROGRAMM_OBRAZ_NM_RED : string;
  end;

  PvstFGOS = ^TvstFGOS;

  TvstFGOS = record
    FGOS_ID: Integer;
    FGOS_NM: string;
//    FGOS_NM2  : string;
  end;


  /// <summary>
  /// ������� ������ � �����
  /// </summary>
function IsGroupInCourse(const Group_code: string): Boolean;
function PosGroupInCourse(const Group_code: string): Integer;

type
  TIndexOfFields_DipGroup = record
    active: Boolean;
    Group_code: Integer;
    Duty_Doc: Integer;
    Duty_Akadem: Integer;
    Duty_Finance: Integer;
    NoDuty_to_Print: Integer;
    Print_need_Blank: Integer;
    Obrazec_not_Confirmed: Integer;
    Is_Archiv_LD: Integer;
    Issue_D_Min,Issue_D_Max: Integer;
  end;

procedure DIP_Group_IOF(q: TIBSQL; var fld: TIndexOfFields_DipGroup);
procedure DIP_Group_Load(q: TIBSQL; var DataC: TvstCourse; var fld: TIndexOfFields_DipGroup);
procedure TvstDiplomZero(var DataD: TvstCertificateInfo);
procedure TvstDiplomClear(var DataD: TvstCertificateInfo);

procedure Course_Programm_Save(TRW: TIBTransaction; var DataCP: TvstProgramm);
function Organization_ID_by_code(const Code: string): Integer;


const
  // ���� "������"
  clRUF_bg_Error = $004F4FFF;
  // ���� "�� ������"
  clRUF_bg_not_found = $0055AAFF;
  SGROUP_FOOTER = '�';

var
  fldDipGroup: TIndexOfFields_DipGroup;

implementation

uses
  Windows, StrUtils, Variants, DateUtils,
  uCommunicationHTTP, IBLib, CustomInsUpd,
  uFregatDB, strJob;

const
  SMACROS_FGOS = '[����]';
  SMACROS_PROGRAMM_OBRAZ = '[��������������� ���������]';
  SMACROS_PROGRAMM_OBRAZ_RED = '[�������� ��������������� ���������]';
  SMACROS_SPECIALIZATION = '[�������������]';
  SMACROS_SUBJECT = '[�������]';

    { TDocument_SN }

function TDocument_SN.DelimiterIndex: Integer;
var
  DIPLOMA_NO: TStringDynArray;
  I: Integer;
begin
  Result := -1;
  StringToList(FNumberFull, DIPLOMA_NO,
          [' ', AnsiNoBreakSpace], [slTrimSpace]);
  for I := 0 to high(DIPLOMA_NO) do
    //������ ������ � ��������� � ������
    if DIPLOMA_NO[I][1]= '�' then
      begin
        Result := I; Break;
      end;
  if Result = -1 then
    begin
      Result := 1;
      if Length(DIPLOMA_NO) = 1 then
        Result := 0
      else
      if Length(DIPLOMA_NO) > 2 then
        for I := Result to high(DIPLOMA_NO) do
          //��������� �������� � �����, �������� � ������
          if IsDigital(DIPLOMA_NO[i][1]) then
          begin
            Result := I; Break;

          end;
    end;
end;

function TDocument_SN.GetNumber: string;
var
  DIPLOMA_NO: TStringDynArray;
  I,N: Integer;
begin
  Result := '';
  StringToList(FNumberFull, DIPLOMA_NO,
          [' ', #9, AnsiNoBreakSpace], [slTrimSpace]);
  N := DelimiterIndex();

  Result := '';
  for I := N to high(DIPLOMA_NO) do
    begin
      if DIPLOMA_NO[I][1]='�' then
        begin
          Delete(DIPLOMA_NO[I], 1, 1);
          DIPLOMA_NO[I] := trim(DIPLOMA_NO[I]);
        end;
      if DIPLOMA_NO[I] = '' then
        Continue;

      if Result <> '' then
        Result := Result + AnsiNoBreakSpace;

      Result := Result + DIPLOMA_NO[I];
    end;
  SetLength(DIPLOMA_NO,0);
end;

function TDocument_SN.GetSeria: string;
var
  DIPLOMA_NO: TStringDynArray;
  I,N: Integer;
begin
  Result := '';
  StringToList(FNumberFull, DIPLOMA_NO,
          [' ', AnsiNoBreakSpace], [slTrimSpace]);
  if Length(DIPLOMA_NO) > 1 then
    begin
      N := DelimiterIndex();
      for I := 0 to N-1 do
        begin
          if Result <> '' then
            Result := Result + AnsiNoBreakSpace;

          Result := Result + DIPLOMA_NO[I];
        end;
      //Result := DIPLOMA_NO[0];
      SetLength(DIPLOMA_NO,0);
    end;
end;

procedure TDocument_SN.SetNumberFull(const Value: string);
begin
  FNumberFull := Value;
end;


procedure Course_Programm_Save(TRW: TIBTransaction; var DataCP: TvstProgramm);
var
  iu: TInsUpd;
begin
  if DataCP.COURSE_PROGRAMM_ID = 0 then
    DataCP.COURSE_PROGRAMM_ID := Get_UniqueKey(TRW, 'GEN_DIP_COURSE_PROGRAMM_ID');
  iu := TInsUpd.Create(nil, TRW, kiuInsertUpdate, 'DIP_COURSE_PROGRAMM');
  try
    iu.AddWhere('COURSE_PROGRAMM_ID', DataCP.COURSE_PROGRAMM_ID);
    iu.ValueStr('COURSE_PROGRAMM_NM', DataCP.COURSE_PROGRAMM_NM, True);
    iu.ValueStr('COURSE_PROGRAMM_PRINT', DataCP.COURSE_PROGRAMM_PRINT, True);
    iu.ExecSQL;
  finally
    iu.Free;
  end;
end;

function PosGroupInCourse(const Group_code: string): Integer;
begin
  Result := Pos('/', Group_code);
end;

function IsGroupInCourse(const Group_code: string): Boolean;
begin
  // ������� ������� ������ ��� �����������
  Result := PosGroupInCourse(Group_code) > 0;
end;

procedure TvstDiplomZero(var DataD: TvstCertificateInfo);
begin
  ZeroMemory(@DataD, SizeOf(TvstCertificateInfo));
end;

procedure TvstDiplomClear(var DataD: TvstCertificateInfo);
begin
  Finalize(DataD);
  TvstDiplomZero(DataD);
end;

{ TRSK_Code }

procedure TRSK_Code.Build;
begin
  FCode := Course_cd;
  if Group_Number <> '' then
    begin
      FCode := FCode + '/' + Group_Number;
      Group_cd := FCode;
    end;
end;

procedure TRSK_Code.Clear;
begin
  Course_ID    :=0;
  Group_Id     :=0;
end;

function TRSK_Code.GetAutoCourse_type_ID: Integer;
var
  Code3: string;
begin
  Code3 := copy(Self.CodeU,1,3);
  if Code3='���' then
    Result := 1
  else
  if Code3='���' then
    Result := 2
  else
  if Code3='���' then
    Result := 3
  else
  if (Code3='���') and (Code3='���') then
    Result := 4
  else
    Result := 0;
end;

function TRSK_Code.GetCode: string;
begin
  Result := FCode;
end;

function TRSK_Code.GetCodeC: string;
begin

end;

function TRSK_Code.GetIsFooter: Boolean;
begin
  Result := Group_Number = SGROUP_FOOTER;
end;

function TRSK_Code.GetIsGroupInCourse: Boolean;
begin
  Result := Group_Number <> '';
end;

function TRSK_Code.GetIsSubCourse: Boolean;
begin
  Result := Course_cd <> SubCourse_cd;
end;

function TRSK_Code.PosGroupInCourse(const Group_code: string): Integer;
begin
  Result := Pos('/', Group_code);
end;

function TRSK_Code.GetCodeU: string;
begin
  Result := AnsiUpperCase(Code);
end;

function TRSK_Code.GetCodeURL: AnsiString;
begin
  Result := UrlEncode(AnsiToUtf8(Code));
end;

function TRSK_Code.GetCourseFromCode(const Group_code: string): string;
var
  tDelimiter: Integer;
begin
  tDelimiter := PosGroupInCourse(Group_code);
  Result := Copy(Group_code, 1, tDelimiter - 1);
end;

function TRSK_Code.GetCourseIsGroup: Boolean;
begin
  Result := IsCorrect and (Course_cd = Group_cd);
end;

procedure TRSK_Code.Make_Course_ID(TRW: TIBTransaction);
begin
  if Course_ID <> 0 then Exit;

  Course_ID := GetValueFieldInt(TRW,
        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_DIP_COURSE'')');
end;

procedure TRSK_Code.SetCode(const Value: string);
begin
  //������ ���������� ����, ������� ��������� �������������
  if FCode = Value then
    Exit;

  Clear;

  CodeC := Value;
end;

procedure TRSK_Code.SetCodeC(const Value: string);
var
  t, tSlash : Integer;
begin
  // ���������� ������ ����
  FCode := AnsiUpperCase(Trim(Value));
  t := Pos('-', FCode);
  if t = 0 then
  begin
    Course_type := '';
  end
  else
  begin
    Course_type := Copy(FCode, 1, t - 1);
    if Length(Course_type) > 3 then
    begin
      Course_offline := copy(Course_type, 4, length(Course_type));
      if Pos('�', Course_offline) > 0 then
        Course_offline := '�'
      else
        Course_offline := '';
    end
    else
      Course_offline := '';
    FCourse_Number := copy(FCode, t + 1, Length(FCode));
    IsCorrect := (t in [4..6]);

    tSlash := Pos('/', FCourse_Number);
    if tSlash > 0 then
      SetLength(FCourse_Number, tSlash - 1);

  end;

  FPosGroup := Pos('/', FCode);
  if not IsCorrect or (FPosGroup = 0) then
  begin
    Course_cd := FCode;
    Group_cd := '';
    FGroup_Number := '';
  end
  else
  begin
    Course_cd := Copy(FCode, 1, FPosGroup - 1);
    Group_cd := FCode;
    FGroup_Number := AnsiLowerCase(Copy(FCode, FPosGroup + 1, Length(FCode)));
    if FGroup_Number = '' then
    begin
      IsCorrect := False;
      Group_cd := '';
    end;
  end;
  t := Pos('.', Course_cd);
  if t = 0 then
    SubCourse_cd := Course_cd
  else
    SubCourse_cd := Copy(Course_cd, 1, t - 1);

  Build;
  if IsCorrect and (AnsiUpperCase(Trim(Value)) <> AnsiUpperCase(FCode)) then
    raise Exception.Create(format('������ ������������ TRSK_Code.SetCode "%s" "%s"',
      [Value, FCode]));

  if not IsCorrect then
    IsActive := False;
end;

procedure TRSK_Code.SetCourse_ID(const Value: Integer);
begin
  FCourse_ID := Value;
end;

procedure TRSK_Code.SetCourse_Number(const Value: string);
begin
  FCourse_Number := Value;
end;

procedure TRSK_Code.SetCourse_Offline(const Value: string);
begin
  FCourse_Offline := Value;
end;

procedure TRSK_Code.SetCourse_Type_Id(const Value: Integer);
begin
  FCourse_Type_Id := Value;
end;

procedure TRSK_Code.SetGroup_ID(const Value: Integer);
begin
  FGroup_ID := Value;
end;

procedure TRSK_Code.SetGroup_Number(const Value: string);
begin
  FGroup_Number := Value;
  Build;
end;

{ TOrganization }

procedure TOrganization.SetORGANIZATION_ID(const Value: Integer);
begin
  FORGANIZATION_ID := Value;
end;

destructor TCourse_Type.Destroy;
begin

  inherited;
end;

procedure TCourse_Type.SetCourse_Type_ID(const Value: Integer);
begin
  FCourse_Type_ID := Value;
end;

{ TvstDCourses }

constructor TCourse_Group.Create;
begin
  inherited;
  FORDER_OF_DISMISSAL  := '';
  FORDER_OF_ENROLLMENT := '';
end;

function TCourse_Group.Getis_Learn_Dates: Boolean;
begin
  // ���� �������� ��������
  Result := (D_LEARN_START > 0) and
            (D_LEARN_END > 0);
end;

function TCourse_Group.GetOpen_State_Id: Integer;
begin
  Result := FStatusGroup.Open_State_Id;
end;

function TCourse_Group.GetSD_ISSUE: string;
begin
  if (D_ISSUE > 0) then
    Result := DateToStr(D_ISSUE)
  else
    Result := '';
end;

function TCourse_Group.GetSD_LEARN_END: string;
begin
  // TODO -cMM: TCourse_Group.GetSD_LEARN_END default body inserted
  if (D_LEARN_END > 0) then
    Result := DateToStr(D_LEARN_END)
  else
    Result := '';
end;

function TCourse_Group.GetSD_LEARN_START: string;
begin
  // TODO -cMM: TCourse_Group.GetSD_LEARN_START default body inserted
  if (D_LEARN_START > 0) then
    Result := DateToStr(D_LEARN_START)
  else
    Result := '';
end;

procedure TCourse_Group.Save;
begin

end;

procedure TCourse_Group.SetBlank_type(const Value: Integer);
begin
  FBlank_type := Value;
end;

procedure TCourse_Group.SetDECREE_OF_DISMISSAL_ID(const Value: Integer);
begin
  if FDECREE_OF_DISMISSAL_ID <> Value then
    Modify := True;
  FDECREE_OF_DISMISSAL_ID := Value;
end;

procedure TCourse_Group.SetDECREE_OF_ENROLLMENT_ID(const Value: Integer);
begin
  if FDECREE_OF_ENROLLMENT_ID <> Value then
    Modify := True;
  FDECREE_OF_ENROLLMENT_ID := Value;
end;

procedure TCourse_Group.SetDuty_Akadem(const Value: TStatus_Duty_Akadem);
begin
  if FDuty_Akadem = Value then
    Exit;
  FDuty_Akadem := Value;
  FDuty_Change_d := Now();
  Modify := True;
end;

procedure TCourse_Group.SetDuty_Change_d(const Value: TDateTime);
begin
  FDuty_Change_d := Value;
end;

procedure TCourse_Group.SetDuty_Doc(const Value: Boolean);
begin
  if FDuty_Doc = Value then
    Exit;
  FDuty_Doc := Value;
  FDuty_Change_d := Now();
  Modify := True;
end;

procedure TCourse_Group.SetDuty_Finance(const Value: Boolean);
begin
  if FDuty_Finance = Value then
    Exit;
  FDuty_Finance := Value;
  FDuty_Change_d := Now();
  Modify := True;
end;

procedure TCourse_Group.SetD_ISSUE(const Value: TDateTime);
begin
  FD_ISSUE := Value;
end;

procedure TCourse_Group.SetD_LEARN_END(const Value: TDateTime);
begin
  if FD_LEARN_END <> Value then
    Modify := True;
  FD_LEARN_END := Value;
end;

procedure TCourse_Group.SetD_LEARN_START(const Value: TDateTime);
begin
  if FD_LEARN_START <> Value then
    Modify := True;
  FD_LEARN_START := Value;
end;

procedure TCourse_Group.SetIn_Volume_Hours(const Value: Integer);
begin
  if FIn_Volume_Hours <> Value then
    Modify := True;
  FIn_Volume_Hours := Value;
end;

procedure TCourse_Group.SetIssue_D_Max(const Value: TDateTime);
begin
  if FIssue_D_Max <> Value then
    Modify := True;
  FIssue_D_Max := Value;
end;

procedure TCourse_Group.SetIssue_D_Min(const Value: TDateTime);
begin
  if FIssue_D_Min <> Value then
    Modify := True;
  FIssue_D_Min := Value;
end;

procedure TCourse_Group.SetIs_Archiv_LD(const Value: Boolean);
begin
  if FIs_Archiv_LD <> Value then
    Modify := True;
  FIs_Archiv_LD := Value;
end;

procedure TCourse_Group.SetLast_Import_Statement_D(const Value: TDateTime);
begin
  if FLast_Import_Statement_D = Value then
    Exit;
  FLast_Import_Statement_D := Value;
  Modify := True;
end;

procedure TCourse_Group.SetModify(const Value: Boolean);
begin
  FModify := Value;
end;

procedure TCourse_Group.SetNoDuty_to_Print(const Value: Boolean);
begin
  if FNoDuty_to_Print = Value then
    Exit;
  FNoDuty_to_Print := Value;
  Modify := True;
end;

procedure TCourse_Group.SetObrazec_not_Confirmed(const Value: Boolean);
begin
  if FObrazec_not_Confirmed = Value then
    Exit;
  FObrazec_not_Confirmed := Value;
  Modify := True;
end;

procedure TCourse_Group.SetOpen_State_Id(const Value: Integer);
begin
  FStatusGroup.Open_State_Id := Value;
end;

procedure TCourse_Group.SetORDER_OF_DISMISSAL(const Value: string);
begin
  FORDER_OF_DISMISSAL := Value;
end;

procedure TCourse_Group.SetORDER_OF_ENROLLMENT(const Value: string);
begin
  FORDER_OF_ENROLLMENT := Value;
end;

procedure TCourse_Group.SetPath_Archiv_ID(const Value: Integer);
begin
  FPath_Archiv_ID := Value;
end;

procedure TCourse_Group.SetPath_Archiv_NM(const Value: string);
begin
  FPath_Archiv_NM := Value;
end;

procedure TCourse_Group.SetPrint_need_Blank(const Value: Boolean);
begin
  if FPrint_need_Blank = Value then
    Exit;
  FPrint_need_Blank := Value;
  Modify := True;
end;

procedure TCourse_Group.SetProfession_nm(const Value: string);
begin
  FProfession_nm := Value;
end;

procedure TCourse_Group.SetSCAN_PATH_ID(const Value: Integer);
begin
  FSCAN_PATH_ID := Value;
end;

procedure TCourse_Group.SetSCAN_PATH_NM(const Value: string);
begin
  FSCAN_PATH_NM := Trim(Value);

  if (FSCAN_PATH_NM <> '') and
       not IsPathDelimiter(FSCAN_PATH_NM, Length(FSCAN_PATH_NM)) then
    FSCAN_PATH_NM := IncludeTrailingPathDelimiter(FSCAN_PATH_NM);

end;

procedure TCourse_Group.SetSend_to_FRDO(const Value: Boolean);
begin
  if FSend_to_FRDO = Value then
    Exit;
  FSend_to_FRDO := Value;
  Modify := True;
end;

procedure TCourse_Group.SetSIGNATURE_ID(const Value: Integer);
begin
  FSIGNATURE_ID := Value;
end;

procedure TCourse_Group.SetSIGNATURE_KOMISSION_ID(const Value: Integer);
begin
  FSIGNATURE_KOMISSION_ID := Value;
end;

procedure TCourse_Group.SetStatusGroup(const Value: TStatusGroup);
begin
  FStatusGroup := Value;
end;

procedure TCourse_Group.SetTransferred_to_UchMag(const Value: Boolean);
begin
  if FTransferred_to_UchMag = Value then
    Exit;
  FTransferred_to_UchMag := Value;
  Modify := True;
end;

procedure TDIPCourse.CourseProgramm_Save(TRW: TIBTransaction);
begin
  Course_Programm_Save(TRW, FCOURSE_PROGRAMM);
end;

constructor TDIPCourse.Create;
begin
  FLast_Import_Statement_D := null;
  FLast_Uchmet_Statement_D := null;
  FLast_Work_Grade_D       := null;
  FStudents_Outside_the_group := False;
end;

destructor TDIPCourse.Destroy;
begin
  if Assigned(CourseDisciplines) then
    CourseDisciplines.Free;
end;


//procedure TDIPCourse.SetCOURSE_PROGRAMM_ID(const Value: Integer);
//begin
//  FCOURSE_PROGRAMM_ID := Value;
//end;
//
//procedure TDIPCourse.SetCOURSE_PROGRAMM_NM(const Value: string);
//begin
//  FCOURSE_PROGRAMM_NM := Value;
//end;

procedure TDIPCourse.SetCOURSE_PROGRAMM(const Value: TvstProgramm);
begin
  FCOURSE_PROGRAMM := Value;
end;

procedure TDIPCourse.SetCOURSE_PROGRAMM_ID(const Value: Integer);
begin
  FCOURSE_PROGRAMM.COURSE_PROGRAMM_ID := Value;
end;

procedure TDIPCourse.SetCOURSE_PROGRAMM_NM(const Value: string);
begin
  FCOURSE_PROGRAMM.COURSE_PROGRAMM_NM := Value;
end;

procedure TDIPCourse.SetForm_of_Implementation(const Value: Integer);
begin
  FForm_of_Implementation := Value;
end;

procedure TDIPCourse.SetIN_VOLUME_HOURS(const Value: Integer);
begin
  if FIN_VOLUME_HOURS = Value then
    Exit;
  FIN_VOLUME_HOURS := Value;
  Modify := True;
end;

procedure TDIPCourse.SetIs_Practice(const Value: Boolean);
begin
  FIs_Practice := Value;
end;

function TDIPCourse.GetCOURSE_PROGRAMM_ID: Integer;
begin
  Result := COURSE_PROGRAMM.COURSE_PROGRAMM_ID;
end;

function TDIPCourse.GetCOURSE_PROGRAMM_NM: string;
begin
  Result := COURSE_PROGRAMM.COURSE_PROGRAMM_NM
end;

function TDIPCourse.GetIs_ContainsFGOS: Boolean;
begin
  Result := (Pos_ContainsFGOS <> 0);
end;

function TDIPCourse.GetIs_ContainsProgramm_Obraz: Boolean;
begin
  Result := (Pos_ContainsProgramm_Obraz <> 0);
end;

function TDIPCourse.GetIs_ContainsSpecialization: Boolean;
begin
  Result := (Pos_ContainsSpecialization <> 0);
end;

function TDIPCourse.GetIs_ContainsSubject: Boolean;
begin
  Result := (Pos_ContainsSubject <> 0);
end;

function TDIPCourse.GetLast_Grade_Min: TDateTime;
begin
  // TODO -cMM: TDIPCourse.GetLast_Grade_Min default body inserted
  if FLast_Work_Grade_D = null then
    Result := 0
  else
    Result := FLast_Work_Grade_D;

  if Result = 0 then
    Result := FLast_Uchmet_Grade_D
  else
  if Result > FLast_Uchmet_Grade_D then
    Result := FLast_Uchmet_Grade_D;

end;

function TDIPCourse.GetNeed_Import_Grade: Boolean;
begin
  Result := (FLast_Uchmet_Grade_D > 0) and
            ((FLast_Work_Grade_D = null) or
             (FLast_Uchmet_Grade_D > FLast_Work_Grade_D));
end;

function TDIPCourse.GetNeed_Import_Statement: Boolean;
begin
  Result := ((FLast_Import_Statement_D<>null) and (FLast_Uchmet_Statement_D<>null) and
             (FLast_Import_Statement_D > 0) and
             (DateOf(FLast_Import_Statement_D) < DateOf(FLast_Uchmet_Statement_D)))
            or
            ( ((FLast_Import_Statement_D = null) or (FLast_Import_Statement_D = 0)) and
              (FLast_Uchmet_Statement_D<>null) and (FLast_Uchmet_Statement_D > 0)

            )
end;

function TDIPCourse.GetPos_ContainsFGOS: Integer;
begin
  Result := pos(SMACROS_FGOS, AnsiUpperCase(COURSE_PROGRAMM.COURSE_PROGRAMM_NM));
end;

function TDIPCourse.GetPos_ContainsProgramm_Obraz: Integer;
begin
  Result := pos(SMACROS_PROGRAMM_OBRAZ, AnsiUpperCase(COURSE_PROGRAMM.COURSE_PROGRAMM_NM));
end;

function TDIPCourse.GetPos_ContainsSpecialization: Integer;
begin
  Result := pos(SMACROS_SPECIALIZATION, AnsiUpperCase(COURSE_PROGRAMM.COURSE_PROGRAMM_NM));
end;

function TDIPCourse.GetPos_ContainsSubject: Integer;
begin
  Result := pos(SMACROS_SUBJECT, AnsiUpperCase(COURSE_PROGRAMM.COURSE_PROGRAMM_NM));
end;

function TDIPCourse.GetType_of_Instruction_Caption: string;
begin
  // TODO -cMM: TDIPCourse.GetType_of_Instruction_Caption default body inserted
  case FType_of_Instruction of
  1: Result := '������';
  2: Result := '������';
  3: Result := '���������';
  else
     Result := '';
  end;
end;

function TDIPCourse.ProgrammMacros(const AFGOS, ASubject,ASpecialization,
  AProgramm_obraz,AProgramm_obraz_red: string): string;
//var
//  t: Integer;
begin
  Result := COURSE_PROGRAMM.COURSE_PROGRAMM_NM;
  if AFGOS <> '' then
    Result := StringReplace(Result, SMACROS_FGOS, AFGOS,
                            [rfReplaceAll, rfIgnoreCase]);
  if ASubject<>'' then
    Result := StringReplace(Result, SMACROS_SUBJECT, ASubject,
                            [rfReplaceAll, rfIgnoreCase]);
  if ASpecialization<>'' then
    Result := StringReplace(Result, SMACROS_SPECIALIZATION, ASpecialization,
                            [rfReplaceAll, rfIgnoreCase]);
  if AProgramm_obraz<>'' then
    begin
      Result := StringReplace(Result, SMACROS_PROGRAMM_OBRAZ, AProgramm_obraz,
                              [rfReplaceAll, rfIgnoreCase]);
      Result := StringReplace(Result, SMACROS_PROGRAMM_OBRAZ_RED, AProgramm_obraz_red,
                              [rfReplaceAll, rfIgnoreCase]);
    end;


end;

procedure TDIPCourse.SetLast_Uchmet_Grade_D(const Value: TDateTime);
begin
  if FLast_Uchmet_Grade_D = Value then
    Exit;
  FLast_Uchmet_Grade_D := Value;
  Modify := True;
end;

procedure TDIPCourse.SetLast_Import_Statement_D(const Value: Variant);
begin
  FLast_Import_Statement_D := Value;
end;

procedure TDIPCourse.SetLast_Work_Grade_D(const Value: Variant);
begin
  if VarSameValue( FLast_Work_Grade_D, Value ) then
    Exit;
  if (Value <> null) and (Value = 0) and (FLast_Work_Grade_D = null) then
    // ������ ������������ �� ���������� �.�. ����� ���������� ��� �� �������
    // ���� ������ ��������� � �� ��� �������
    //Modify := False
  else
    Modify := True;

  FLast_Work_Grade_D := Value;
end;

procedure TDIPCourse.SetLast_Uchmet_Statement_D(const Value: Variant);
var
  V: Variant;
begin
  V := Value;
  if (V <> null) and (V = 0) then
    V := null;
  if VarSameValue(FLast_Uchmet_Statement_D, V) then
    Exit;
  FLast_Uchmet_Statement_D := Value;
  Modify := True;
end;

procedure TDIPCourse.SetModify(const Value: Boolean);
begin
  FModify := Value;
end;

procedure TDIPCourse.SetORGANIZATION_ID(const Value: Integer);
begin
  if FORGANIZATION_ID = Value then
    Exit;
  FORGANIZATION_ID := Value;
  FModify := True;
end;

procedure TDIPCourse.SetRegNumberPostfix(const Value: string);
begin
  if FRegNumberPostfix = Value then
    Exit;

  FRegNumberPostfix := Value;
  FModify := True;
end;

procedure TDIPCourse.SetRegNumberPrefix(const Value: string);
begin
  if FRegNumberPrefix = Value then
    Exit;

  FRegNumberPrefix := Value;
  FModify := True;
end;

procedure TDIPCourse.SetQUALIFICATION_ID(const Value: Integer);
begin
  if FQUALIFICATION_ID = Value then
    Exit;

  FQUALIFICATION_ID := Value;
  FModify := True;
end;

procedure TDIPCourse.SetREG_NUMBER_TEMPL_ID(const Value: Integer);
begin
  if FREG_NUMBER_TEMPL_ID = Value then
    Exit;

  FREG_NUMBER_TEMPL_ID := Value;
  FModify := True;
end;

procedure TDIPCourse.SetSPHERE_OF_ACTIVITY_ID(const Value: Integer);
begin
  if FSPHERE_OF_ACTIVITY_ID = Value then
    Exit;

  FSPHERE_OF_ACTIVITY_ID := Value;
  FModify := True;
end;

procedure TDIPCourse.SetSPHERE_OF_ACTIVITY_NM(const Value: string);
begin
  if FSPHERE_OF_ACTIVITY_NM = Value then
    Exit;
  FSPHERE_OF_ACTIVITY_NM := Value;
  FModify := True;
end;

procedure TDIPCourse.SetStudents_Outside_the_group(const Value: Boolean);
begin
  if FStudents_Outside_the_group = Value then
    Exit;

  FStudents_Outside_the_group := Value;
  FModify := True;
end;

procedure TDIPCourse.SetType_of_Instruction(const Value: Integer);
begin
  FType_of_Instruction := Value;
end;

procedure TvstCourse.Assign(const Source: TvstCourse);
begin
  if Assigned(Source.Course_Group) then
    begin
      if not Assigned(Self.Course_Group) then
        Self.Course_Group := TCourse_Group.Create;

      Self.Course_Group.D_LEARN_START := Source.Course_Group.D_LEARN_START;
      Self.Course_Group.D_LEARN_END := Source.Course_Group.D_LEARN_END;
      Self.Course_Group.D_ISSUE := Source.Course_Group.D_ISSUE;
      Self.Course_Group.SIGNATURE_ID := Source.Course_Group.SIGNATURE_ID;
      Self.Course_Group.SIGNATURE_KOMISSION_ID := Source.Course_Group.SIGNATURE_KOMISSION_ID;
      Self.Course_Group.In_Volume_Hours := Source.Course_Group.In_Volume_Hours;
      Self.Course_Group.Blank_type := Source.Course_Group.Blank_type;
      Self.Course_Group.SCAN_PATH_ID := Source.Course_Group.SCAN_PATH_ID;
      Self.Course_Group.SCAN_PATH_NM := Source.Course_Group.SCAN_PATH_NM;
      Self.Course_Group.Open_State_Id := Source.Course_Group.Open_State_Id;


    end;

end;

function TvstCourse.Getis_Learn_Dates: Boolean;
begin
  Result := (Assigned(Course_Group) and Course_Group.is_Learn_Dates);
end;

function TvstCourse.GetWrongOption: Boolean;
begin
  Result := (Assigned(Course) and (Course.ORGANIZATION_ID = 0)) or
            (Assigned(Course_Group) and ((Course_Group.Blank_type = 0) or
            (Course_Group.SIGNATURE_ID = 0)));

end;

procedure DIP_Group_IOF(q: TIBSQL; var fld: TIndexOfFields_DipGroup);
begin
  fld.Group_code := q.FieldIndex['Group_nm'];
  fld.Duty_Doc := q.FieldIndex['Duty_Doc'];
  fld.Duty_Akadem := q.FieldIndex['Duty_Akadem'];
  fld.Duty_Finance := q.FieldIndex['Duty_Finance'];
  fld.NoDuty_to_Print := q.FieldIndex['NoDuty_to_Print'];
  fld.Print_need_Blank := q.FieldIndex['Print_need_Blank'];
  fld.Obrazec_not_Confirmed := q.FieldIndex['Obrazec_not_Confirmed'];
  fld.Is_Archiv_LD := q.FieldIndex['Is_Archiv_LD'];
  fld.Issue_D_Min := q.FieldIndex['Issue_D_Min'];
  fld.Issue_D_Max := q.FieldIndex['Issue_D_Max'];


  fld.Active := True;
end;

procedure DIP_Group_Load(q: TIBSQL; var DataC: TvstCourse; var fld: TIndexOfFields_DipGroup);
begin
  DataC.RSK_Code.CodeC := q.FieldByName('GROUP_NM').AsString;
  if DataC.RSK_Code.Course_ID = 0 then
    DataC.RSK_Code.Course_ID := q.FieldByName('Course_ID').AsInteger;
  if DataC.RSK_Code.Group_ID = 0 then
    DataC.RSK_Code.Group_ID := q.FieldByName('Group_ID').AsInteger;

  // ������� ������ ������
  if DataC.Course_Group = nil then
    DataC.Course_Group := TCourse_Group.Create;

  DataC.Course_Group.D_LEARN_START := q.FieldByName('D_LEARN_START').AsDate;
  DataC.Course_Group.D_LEARN_END := q.FieldByName('D_LEARN_END').AsDate;
  DataC.Course_Group.D_ISSUE := q.FieldByName('ISSUE_DT').AsDate;

  DataC.Course_Group.ORDER_OF_ENROLLMENT := q.FieldByName('ORDER_OF_ENROLLMENT').AsString;
  DataC.Course_Group.ORDER_OF_DISMISSAL := q.FieldByName('ORDER_OF_DISMISSAL').AsString;

  DataC.Course_Group.DECREE_OF_ENROLLMENT_ID := q.FieldByName('DECREE_OF_ENROLLMENT_ID').AsInteger;
  DataC.Course_Group.DECREE_OF_DISMISSAL_ID := q.FieldByName('DECREE_OF_DISMISSAL_ID').AsInteger;

  DataC.Course_Group.In_Volume_Hours := q.FieldByName('In_Volume_Hours').AsInteger;
  DataC.Course_Group.SIGNATURE_ID := q.FieldByName('SIGNATURE_ID').AsInteger;
  DataC.Course_Group.SIGNATURE_KOMISSION_ID := q.FieldByName('SIGNATURE_KOMISSION_ID').AsInteger;
//  DataC.Course_Group.Organization_ID :=
//    q.FieldByName('ORGANIZATION_ID').AsInteger;
  DataC.Course_Group.Blank_type := q.FieldByName('Blank_type').AsInteger;
  // �����
  DataC.Course_Group.SCAN_PATH_ID := q.FieldByName('SCAN_PATH_ID').AsInteger;
  DataC.Course_Group.SCAN_PATH_NM := q.FieldByName('SCAN_PATH_NM').AsString;
  DataC.Course_Group.Open_State_Id := q.FieldByName('OPEN_STATE_ID').AsInteger;
  DataC.Course_Group.Duty_Doc := q.Fields[fld.Duty_Doc].AsInteger = 1;
  DataC.Course_Group.Duty_Akadem := TStatus_Duty_Akadem(q.Fields[fld.Duty_Akadem].AsInteger);
  DataC.Course_Group.Duty_Finance := q.Fields[fld.Duty_Finance].AsInteger = 1;
  DataC.Course_Group.NoDuty_to_Print := q.Fields[fld.NoDuty_to_Print].AsInteger = 1;
  DataC.Course_Group.Print_need_Blank := q.Fields[fld.Print_need_Blank].AsInteger = 1;
  DataC.Course_Group.Obrazec_not_Confirmed := q.Fields[fld.Obrazec_not_Confirmed].AsInteger = 1;

  DataC.Course_Group.Duty_Change_d := q.FieldByName('Duty_Change_d').AsDate;
  DataC.Course_Group.Last_Import_Statement_D := q.FieldByName('Last_Import_Statement_D').AsDateTime;
  DataC.Course_Group.Transferred_to_UchMag := q.FieldByName('Transferred_to_UchMag').AsInteger = 1;
  DataC.Course_Group.Send_to_FRDO := q.FieldByName('Send_to_FRDO').AsInteger = 1;
  DataC.Course_Group.Path_Archiv_ID := q.FieldByName('Path_Archiv_ID').AsInteger;
  DataC.Course_Group.Path_Archiv_NM := q.FieldByName('Path_Archiv_NM').AsString;
  if fld.Is_Archiv_LD >= 0 then
    begin
      DataC.Course_Group.Is_Archiv_LD := q.Fields[fld.Is_Archiv_LD].AsInteger = 1;
      DataC.Course_Group.Issue_D_Min := q.Fields[fld.Issue_D_Min].AsDateTime;
      DataC.Course_Group.Issue_D_Max := q.Fields[fld.Issue_D_Max].AsDateTime;
    end;
  //��������� ������ � ������, ������� ���� ���������
  DataC.Course_Group.Modify := False;
end;

function Organization_ID_by_code(const Code: string): Integer;
begin
  // �������� Id �� ���� �����������
  if (Code='mco') then
    Result := 2
  else
  if (Code='uchitel') then
    Result := 1
  else
    Result := 0;
end;

procedure TvstCertificateInfo.Clear;
begin
  // ������� ������
  Finalize(Self);
end;

procedure TvstCertificateInfo.CopyFrom(const ADataCI: TvstCertificateInfo);
var
  I:Integer;
begin
  Self := ADataCI;
  SetLength(Self.ADDiscipl, Length(ADataCI.ADDiscipl));
  for I := 0 to high(ADataCI.ADDiscipl) do
    Self.ADDiscipl[I] := ADataCI.ADDiscipl[I];
end;

procedure TvstCertificateInfo.PrepareForDublikat;

var
  I: Integer;

begin
  Self.Certificate.Certificate_ID := 0;
  Self.Certificate.Cert_Num := 0;
  Self.Certificate.Printed_Dt := 0;
  Self.Certificate.ISSUE_IN_PERSON_D := 0;
  Self.Certificate.Issue_dt := 0;
  Self.Certificate.Obrazec_Created := 0;
  Self.Certificate.Obrazec_Sent := 0;
  Self.Certificate.Obrazec_Confirmed := 0;
  Self.Certificate.FRDO_D := 0;
  Self.pdf_Count := 0;

  Self.Certificate.Reg_Number := Self.Certificate.Reg_Number + '-�';
  Self.Certificate.PACK_RPO_ID := 0;
//  Self.Certificate.STUDENT_ID := 0;
  Self.DIP_ZAKAZ_INFO.id := 0;
  Self.DIP_ZAKAZ_INFO.PACK_RPO_ID := 0;
  ZeroMemory(@Self.DIP_ZAKAZ_INFO.Pochta, SizeOf(TDip_Pochta));
  Self.DIP_ZAKAZ_INFO.Surnames_RPO := '';
  Self.DIP_ZAKAZ_INFO.IDC_POSTALITEM :='';
  for I := 0 to high(Self.ADDiscipl) do
    Self.ADDiscipl[I].CERTIFICATE_GRADE_ID := 0;
end;

end.

