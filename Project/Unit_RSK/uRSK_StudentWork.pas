unit uRSK_StudentWork;

interface
uses
  {$if CompilerVersion >= 33}
  IBX.IBDatabase
  {$else}
  IBDatabase
  {$ifend}
  ;
type
  TRSK_StudentWork = class
  private
    FTRR: TIBTransaction;
    FSTUDENT_WORK_ID: Integer;
    FCOURSE_DISCIPLINE_ID: Integer;
    FSTUDENT_ID: Integer;
    FCOURSE_ID: Integer;
    FRATING: Integer;
    FGRADE_VALUE_ID: Integer;
    FWORK_DATE: TDateTime;
    FModify: Boolean;
    procedure SetSTUDENT_WORK_ID(const Value: Integer);
    procedure SetCOURSE_DISCIPLINE_ID(const Value: Integer);
    procedure SetSTUDENT_ID(const Value: Integer);
    procedure SetCOURSE_ID(const Value: Integer);
    procedure SetRATING(const Value: Integer);
    procedure SetGRADE_VALUE_ID(const Value: Integer);
    procedure SetWORK_DATE(const Value: TDateTime);
    procedure SetModify(const Value: Boolean);
  public
    constructor Create(ATRR: TIBTransaction; const ACOURSE_ID: Integer);
    destructor Destroy; override;
    procedure Clear;
    procedure Reload(const ACOURSE_DISCIPLINE_ID, ASTUDENT_ID: Integer);
    procedure Save(TRW: TIBTransaction);
    property STUDENT_WORK_ID:Integer read FSTUDENT_WORK_ID write SetSTUDENT_WORK_ID;
    property COURSE_DISCIPLINE_ID: Integer read FCOURSE_DISCIPLINE_ID write SetCOURSE_DISCIPLINE_ID;
    property STUDENT_ID: Integer read FSTUDENT_ID write SetSTUDENT_ID;
    property COURSE_ID : Integer read FCOURSE_ID write SetCOURSE_ID;
    property RATING: Integer read FRATING write SetRATING;
    property GRADE_VALUE_ID: Integer read FGRADE_VALUE_ID write SetGRADE_VALUE_ID;
    property WORK_DATE: TDateTime read FWORK_DATE write SetWORK_DATE;
    property Modify: Boolean read FModify write SetModify;
  end;

implementation

uses
  {$if CompilerVersion >= 33}
  IBX.IBSQL
  {$else}
  IBSQL
  {$ifend}
  , IBLib, CustomInsUpd, uFregatDB;

{ TRSK_StudentWork }

procedure TRSK_StudentWork.Clear;
begin
  FSTUDENT_WORK_ID := 0;
  FCOURSE_DISCIPLINE_ID := 0;
  FSTUDENT_ID := 0;
  FRATING :=0;
  FWORK_DATE := 0;
end;

constructor TRSK_StudentWork.Create(ATRR: TIBTransaction; const ACOURSE_ID:
    Integer);
begin
  inherited Create;
  FTRR := ATRR;
  Clear;
  FCOURSE_ID := ACOURSE_ID;

end;

destructor TRSK_StudentWork.Destroy;
begin

  inherited;
end;

procedure TRSK_StudentWork.Reload(const ACOURSE_DISCIPLINE_ID, ASTUDENT_ID:
    Integer);
var
  q: TIBSQL;
begin
  Clear;
  q := TIBSQL.Create(nil);
  try
    q.Transaction := FTRR;
    q.SQL.Text := 'Select * from DIP_STUDENT_WORK';
    q.SQL.Add('where COURSE_DISCIPLINE_ID=:COURSE_DISCIPLINE_ID and STUDENT_ID=:STUDENT_ID');
    q.ParamByName('COURSE_DISCIPLINE_ID').AsInteger := ACOURSE_DISCIPLINE_ID;
    q.ParamByName('STUDENT_ID').AsInteger := ASTUDENT_ID;
    q.ExecQuery;
    if q.RecordCount = 0 then
      begin
        COURSE_DISCIPLINE_ID := ACOURSE_DISCIPLINE_ID;
        STUDENT_ID := ASTUDENT_ID;
      end
    else
      begin
        STUDENT_WORK_ID:=q.FieldByName('STUDENT_WORK_ID').AsInteger;
        COURSE_DISCIPLINE_ID:=q.FieldByName('COURSE_DISCIPLINE_ID').AsInteger;
        STUDENT_ID:=q.FieldByName('STUDENT_ID').AsInteger;
        COURSE_ID :=q.FieldByName('COURSE_ID').AsInteger;
        RATING:=q.FieldByName('RATING').AsInteger;
        GRADE_VALUE_ID:=q.FieldByName('GRADE_VALUE_ID').AsInteger;
        WORK_DATE:=q.FieldByName('WORK_DATE').AsDateTime;



      end;
    FModify := False;
  finally
    q.Free;
  end;
end;

procedure TRSK_StudentWork.Save(TRW: TIBTransaction);
var
  iu: TInsUpd;
begin
  TRW.Active := True;
  if STUDENT_WORK_ID=0 then
    STUDENT_WORK_ID := Get_UniqueKey(TRW, 'GEN_DIP_STUDENT_WORK');

  iu := TInsUpd.Create(nil, TRW, kiuInsertUpdate, 'DIP_STUDENT_WORK');
  try
    iu.AddWhere('STUDENT_WORK_ID',STUDENT_WORK_ID);
    iu.ValueInt('COURSE_DISCIPLINE_ID',COURSE_DISCIPLINE_ID);

    iu.ValueInt('STUDENT_ID',Student_Id);
    iu.ValueInt('COURSE_ID',COURSE_ID);

    iu.ValueInt('RATING',RATING, False, True);
    iu.ValueInt('GRADE_VALUE_ID',GRADE_VALUE_ID, False, True);
    iu.ValueDate('WORK_DATE', WORK_DATE, True);
    iu.ExecSQL;
  finally
    iu.Free;
  end;
end;

procedure TRSK_StudentWork.SetCOURSE_DISCIPLINE_ID(const Value: Integer);
begin

  FCOURSE_DISCIPLINE_ID := Value;
end;

procedure TRSK_StudentWork.SetCOURSE_ID(const Value: Integer);
begin

  FCOURSE_ID := Value;
end;

procedure TRSK_StudentWork.SetGRADE_VALUE_ID(const Value: Integer);
begin
  if FGRADE_VALUE_ID = Value then Exit;
  FGRADE_VALUE_ID := Value;
  FModify := True;
end;

procedure TRSK_StudentWork.SetModify(const Value: Boolean);
begin
  FModify := Value;
end;

procedure TRSK_StudentWork.SetRATING(const Value: Integer);
begin
  if FRATING = Value then Exit;
  FRATING := Value;
  FModify := True;
end;

procedure TRSK_StudentWork.SetSTUDENT_ID(const Value: Integer);
begin
  FSTUDENT_ID := Value;
end;

procedure TRSK_StudentWork.SetSTUDENT_WORK_ID(const Value: Integer);
begin
  FSTUDENT_WORK_ID := Value;
end;

procedure TRSK_StudentWork.SetWORK_DATE(const Value: TDateTime);
begin
  if FWORK_DATE = Value then Exit;
  FWORK_DATE := Value;
  FModify := True;
end;

end.
