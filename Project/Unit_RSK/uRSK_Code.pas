unit uRSK_Code;

interface
uses
  {$if CompilerVersion >= 33}
  IBX.IBDatabase
  {$else}
  IBDatabase
  {$ifend}
  ;
type
  TRSK_Code = record
    Course_type  : string;
    Course_cd    : string;
    SubCourse_cd : string;
    Group_cd     : string;
    FGroup_Number : string;
    IsCorrect    : Boolean;
    IsActive     : Boolean;
  private
    FCode        : string;
    FPosGroup    : Integer;
    FCourse_Type_Id : Integer;
    FCourse_ID    : Integer;
    FGroup_Id     : Integer;
    FCourse_Number: string;
    FCourse_Offline: string;
    function GetCode: string;
    procedure SetCode(const Value: string);
    function GetIsGroupInCourse: Boolean;
    procedure SetCourse_Type_Id(const Value: Integer);
    procedure SetCourse_ID(const Value: Integer);
    function GetCourseIsGroup: Boolean;
    procedure SetGroup_ID(const Value: Integer);
    function GetIsSubCourse: Boolean;
    function GetCodeU: string;
    procedure Build;
    procedure SetGroup_Number(const Value: string);
    procedure SetCourse_Number(const Value: string);
    procedure SetCourse_Offline(const Value: string);
    function GetCodeC: string;
    procedure SetCodeC(const Value: string);
    function GetCodeURL: AnsiString;
    function GetIsFooter: Boolean;
    function PosGroupInCourse(const Group_code: string): Integer;
    function GetAutoCourse_type_ID: Integer;
  public
    procedure Clear;
//    procedure Make_Course_ID(TRW: TIBTransaction);
    function GetCourseFromCode(const Group_code: string): string;
    property Code: string read GetCode write SetCode;
    property CodeU: string read GetCodeU;
    property CodeC: string read GetCodeC write SetCodeC;
    property CodeURL: AnsiString read GetCodeURL;
    property IsGroupInCourse: Boolean read GetIsGroupInCourse;
    property IsSubCourse: Boolean read GetIsSubCourse;
    property IsFooter: Boolean read GetIsFooter;
    property CourseIsGroup: Boolean read GetCourseIsGroup;
    property Course_Type_Id: Integer read FCourse_Type_Id write SetCourse_Type_Id;
    property Course_ID: Integer read FCourse_ID write SetCourse_ID;
    property Group_ID: Integer read FGroup_ID write SetGroup_ID;
    property Group_Number : string read FGroup_Number write SetGroup_Number;
    property Course_Number: string read FCourse_Number write SetCourse_Number;
    property Course_Offline: string read FCourse_Offline write SetCourse_Offline;
    property AutoCourse_type_ID: Integer read GetAutoCourse_type_ID;
  end;

const
  SGROUP_FOOTER = '�';


implementation
uses
  {$if CompilerVersion >= 33}
  System.SysUtils, NetEncoding
  {$else}
  SysUtils,uCommunicationHTTP
  {$ifend}
  ;

{ TRSK_Code }

procedure TRSK_Code.Build;
begin
  FCode := Course_cd;
  if Group_Number <> '' then
    begin
      FCode := FCode + '/' + Group_Number;
      Group_cd := FCode;
    end;
end;

procedure TRSK_Code.Clear;
begin
  Course_ID    :=0;
  Group_Id     :=0;
end;

function TRSK_Code.GetAutoCourse_type_ID: Integer;
var
  Code3: string;
begin
  Code3 := copy(Self.CodeU,1,3);
  if Code3='���' then
    Result := 1
  else
  if Code3='���' then
    Result := 2
  else
  if Code3='���' then
    Result := 3
  else
  if (Code3='���') and (Code3='���') then
    Result := 4
  else
    Result := 0;
end;

function TRSK_Code.GetCode: string;
begin
  Result := FCode;
end;

function TRSK_Code.GetCodeC: string;
begin

end;

function TRSK_Code.GetIsFooter: Boolean;
begin
  Result := Group_Number = SGROUP_FOOTER;
end;

function TRSK_Code.GetIsGroupInCourse: Boolean;
begin
  Result := Group_Number <> '';
end;

function TRSK_Code.GetIsSubCourse: Boolean;
begin
  Result := Course_cd <> SubCourse_cd;
end;

function TRSK_Code.PosGroupInCourse(const Group_code: string): Integer;
begin
  Result := Pos('/', Group_code);
end;

function TRSK_Code.GetCodeU: string;
begin
  Result := AnsiUpperCase(Code);
end;

function TRSK_Code.GetCodeURL: AnsiString;
begin
  Result := AnsiToUtf8(Code);
  {$if CompilerVersion >= 33}
  Result := TNetEncoding.URL.Encode(Result);
  {$else}
  Result := UrlEncode(Result);
  {$ifend}


end;

function TRSK_Code.GetCourseFromCode(const Group_code: string): string;
var
  tDelimiter: Integer;
begin
  tDelimiter := PosGroupInCourse(Group_code);
  Result := Copy(Group_code, 1, tDelimiter - 1);
end;

function TRSK_Code.GetCourseIsGroup: Boolean;
begin
  Result := IsCorrect and (Course_cd = Group_cd);
end;

//procedure TRSK_Code.Make_Course_ID(TRW: TIBTransaction);
//begin
//  if Course_ID <> 0 then Exit;
//
//  Course_ID := GetValueFieldInt(TRW,
//        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_DIP_COURSE'')');
//end;

procedure TRSK_Code.SetCode(const Value: string);
begin
  //������ ���������� ����, ������� ��������� �������������
  if FCode = Value then
    Exit;

  Clear;

  CodeC := Value;
end;

procedure TRSK_Code.SetCodeC(const Value: string);
var
  t, tSlash : Integer;
begin
  // ���������� ������ ����
  FCode := AnsiUpperCase(Trim(Value));
  t := Pos('-', FCode);
  if t = 0 then
  begin
    Course_type := '';
  end
  else
  begin
    Course_type := Copy(FCode, 1, t - 1);
    if Length(Course_type) > 3 then
    begin
      Course_offline := copy(Course_type, 4, length(Course_type));
      if Pos('�', Course_offline) > 0 then
        Course_offline := '�'
      else
        Course_offline := '';
    end
    else
      Course_offline := '';
    FCourse_Number := copy(FCode, t + 1, Length(FCode));
    IsCorrect := (t in [4..6]);

    tSlash := Pos('/', FCourse_Number);
    if tSlash > 0 then
      SetLength(FCourse_Number, tSlash - 1);

  end;

  FPosGroup := Pos('/', FCode);
  if not IsCorrect or (FPosGroup = 0) then
  begin
    Course_cd := FCode;
    Group_cd := '';
    FGroup_Number := '';
  end
  else
  begin
    Course_cd := Copy(FCode, 1, FPosGroup - 1);
    Group_cd := FCode;
    FGroup_Number := AnsiLowerCase(Copy(FCode, FPosGroup + 1, Length(FCode)));
    if FGroup_Number = '' then
    begin
      IsCorrect := False;
      Group_cd := '';
    end;
  end;
  t := Pos('.', Course_cd);
  if t = 0 then
    SubCourse_cd := Course_cd
  else
    SubCourse_cd := Copy(Course_cd, 1, t - 1);

  Build;
  if IsCorrect and (AnsiUpperCase(Trim(Value)) <> AnsiUpperCase(FCode)) then
    raise Exception.Create(format('������ ������������ TRSK_Code.SetCode "%s" "%s"',
      [Value, FCode]));

  if not IsCorrect then
    IsActive := False;
end;

procedure TRSK_Code.SetCourse_ID(const Value: Integer);
begin
  FCourse_ID := Value;
end;

procedure TRSK_Code.SetCourse_Number(const Value: string);
begin
  FCourse_Number := Value;
end;

procedure TRSK_Code.SetCourse_Offline(const Value: string);
begin
  FCourse_Offline := Value;
end;

procedure TRSK_Code.SetCourse_Type_Id(const Value: Integer);
begin
  FCourse_Type_Id := Value;
end;

procedure TRSK_Code.SetGroup_ID(const Value: Integer);
begin
  FGroup_ID := Value;
end;

procedure TRSK_Code.SetGroup_Number(const Value: string);
begin
  FGroup_Number := Value;
  Build;
end;

end.
