unit uPayment;

interface

uses
  Windows, IBDatabase, IBQuery, IBSQL, Types, StrUtils, Math,
  strJob, uFIO, IBLib, uUM_Address, uIdentifiers_Parcels;

const
  DEBUG_CIZ = 1;
  CPAY_SYSTEM_ID = 1;{$IF CPAY_SYSTEM_ID = 0}{$ELSE}{$IFEND}
type
  TDirectPayment = (dpNone, dpPrihod, dpRashod);

  PPayPostal = ^TPayPostal;

  TPayPostal = record
    KGP_PRIEM: string;
    RPO: string;
    INDEX_PRIEM: string;
    INPO: TRPO_Identifier;
//    IDC_POSTALITEM: string;
    ID_File: Integer;
  end;

  TCash_Inflow_Custom = record
    CASH_INFLOW_CUSTOM_ID: Integer;
    CUSTOM_S : string;
    CUSTOM_NAME: string;
    CUSTOM_INN: string;
    CUSTOM_KPP: string;
    CUSTOM_RS: string;
    CUSTOM_BANK1: string;
    CUSTOM_BIK: string;
    CUSTOM_KS: string;
    CUSTOM_ID: Integer;
  end;

  TCash_inflow = record
    CASH_INFLOW_ID : Integer;
    TYPE_OF_PAYMENT: Integer;
    TYPE_OF_PAYMENT_nm_sml: string;
    D_PAYMENT      : TDateTime;
    D_RECEIPT      : TDateTime;
    N_DOC          : Integer;
    AMOUNT         : Currency;
    FIO_ID         : Integer;
    FIO            : TFIO;
    Contragent     : TCash_Inflow_Custom;
    // ���� �� ���, ��� ����� ������ ����� ������� ��������,
    // ����� ��� � ��������� �����
    FIO_KLIENT     : string;
//    ADDRESS_FREE_ID: Integer;
    Address        : TUM_Address;
    RECIEVER_ID    : Integer;
    // ������ �������
    DETAILS_OF_PAYMENT: string;
    PAYMENT_DAYS_ID : Integer;

//    Section: Integer; //
    Sections: TIntegerDynArray;
    Closed: Boolean;

//    PAYER_INFO_ID: Integer;
  end;

  TCash_inflow_zak = record
    CASH_INFLOW_ZAK_ID: Integer;
    Payment_sum   : Currency;
    ID_Zak        : Integer;
    N_Zak         : string;
    D_Zak         : TDateTime;
    // ��� �������
    FIO_Klient    : string;
    //������� ������ ������
    Canceled      : Boolean;
    //�������, ������ ��������� � ������ �������
    Sent_to_personal_Account: Boolean;
  end;



  TaCash_inflow_zak = array of TCash_inflow_zak;

  PCash_OD = ^TCash_OD;

  TCash_OD = record
    // �������� ������
    ID_CASHONDELIVERY: Integer;
    // � ������ ������ ���������
    ID_ZAK : Integer;
    // ���� �������� ���������� �� ������ � �����
    D_SendUchmag: TDateTime;
    // ������ ����� ������
    Postal: PPayPostal;
    // ����� ������
    // Amount: Currency;
    // ��� �����������
    //Contragent_FIO1: TFIO;

    Address1: TUM_Address;
    Cash_inflow: TCash_inflow;
{$IF DEBUG_CIZ=0}
    Cash_inflow_zak: TCash_inflow_zak;
{$ELSE}
    Cash_inflow_zak: TaCash_inflow_zak;
{$IFEND}
{$IFNDEF VER150} // DELPHI_7
  private
    function GetIS_all_ID_Zak: Boolean;
    function GetIS_any_ID_Zak: Boolean;
  public
    property IS_any_ID_Zak: Boolean read GetIS_any_ID_Zak;
    property IS_all_ID_Zak: Boolean read GetIS_all_ID_Zak;
    function Is_ID_Zak(const ID_zak: Integer): Boolean;
    function IndexCIZ_ByZeroId_Zak: Integer;
    function IndexCIZ_ByMainSum: Integer;
    function IndexCIZ_By_Id(const CIZ_Id:Integer): Integer;
    function GetCIZ_ID_Zak(const Index: Integer): Integer;
{$ENDIF}
  end;

  TCash_OD_IndexOfFields = record
    active: Boolean;
    D_RECEIPT: Integer;
    ID_RECEIVER: Integer;
    TYPE_OF_PAYMENT: Integer;
    Status: Integer;
    ID_PAYMENT_DAYS: Integer;
    NM_RECEIVER: Integer;
    ID_CASHONDELIVERY: Integer;
    D_Priem: Integer;
    N_Priem: Integer;
    AMOUNT: Integer;
    KLIENT_FIO: Integer;
    DETAILS_OF_PAYMENT: Integer;
    ID_zak: Integer;
    N_Zak: Integer;
    NP_ZAK: Integer;
    NC_ZAK: Integer;
    D_Zak: Integer;
    NMKLIENT: Integer;
    ID_Canceled: Integer;
    SEND_READINESS: Integer;
    WO_ORDER: Integer;
    ID_DELIVERY: Integer;
    CASH_INFLOW_ID: Integer;
    CI_FIO_ID: Integer;
    CI_Fio_Klient: Integer;
//    FIO_KLIENT: Integer;
    Fio_Surname: Integer;
    Fio_Name: Integer;
    Fio_Patronymic: Integer;
    CI_ADDRESS_FREE_ID: Integer;
    POST_Index_Priem: Integer;
    Klient_Region: Integer;
    Klient_Address: Integer;
    KAF_ZIP_CODE: Integer;
    KAF_REGION: Integer;
    KAF_ADDRESS: Integer;
    KAF_ADDRESS_FREE: Integer;
    CI_AMOUNT: Integer;
    CI_D_RECEIPT: Integer;
    CI_D_PAYMENT: Integer;
    CI_N_DOC: Integer;
//    CI_TYPE_OF_PAYMENT: Integer;
    CI_RECIEVER_ID: Integer;
    CI_DETAILS_OF_PAYMENT: Integer;
    CI_PAYMENT_DAYS_ID: Integer;
    PAY_SYSTEM_NM_SML: Integer;
    CASH_INFLOW_ZAK_ID: Integer;
    CIZ_Payment_sum: Integer;
    CIZ_ID_ZAK: Integer;
    CIZ_N_ZAK: Integer;
    CIZ_D_Zak: Integer;
    CIZ_SENT_TO_PERSONAL_ACCOUNT: Integer;
    POST_IDC_POSTALITEM: Integer;
    POST_KGP_PRIEM: Integer;

    CI_PAYER_INFO_ID: Integer;
  end;
{$IFDEF VER150}
  // DELPHI_7
function IndexCIZ_ByZeroId_Zak(const Cash_inflow: TCash_inflow;
  const Cash_inflow_zak: TaCash_inflow_zak): Integer;
function IndexCIZ_By_Id(const Cash_inflow_zak: TaCash_inflow_zak;
  const CIZ_Id: Integer): Integer;
function IndexCIZ_ByMainSum(const Cash_inflow: TCash_inflow;
  const Cash_inflow_zak: TaCash_inflow_zak): Integer;
function CIZ_Is_ID_Zak(const Cash_inflow_zak: TaCash_inflow_zak;
  const ID_zak: Integer): Boolean;
function CIZ_ID_ZAK(const Cash_inflow_zak: TaCash_inflow_zak;
  const Index: Integer): Integer;
{$ENDIF}
procedure Cash_OD_Zero(var Payment: TCash_OD);
procedure Cash_OD_Clear(var Payment: TCash_OD);
procedure Clear_Cash_OD(var Payment: TCash_OD); deprecated;
/// ������� ������
procedure CASHONDELIVERY_Save(TRW: TIBTransaction; var Payment: TCash_OD;
  const Links_KIND: Integer; Address: PUM_Address = nil);
procedure CASH_inflow_Save(TRW: TIBTransaction; var Payment: TCash_OD);

procedure CASH_INFLOW_Load(var Payment: TCash_OD; q: TIBQuery;
  const fld: TCash_OD_IndexOfFields; const PayPostal_Load, Load_CI: Boolean); overload;
procedure CASH_INFLOW_Load(var Payment: TCash_OD; q: TIBSQL;
  const fld: TCash_OD_IndexOfFields; const PayPostal_Load, Load_CI: Boolean); overload;

procedure Get_CASH_INFLOW_CUSTOM_ID(TRR: TIBTransaction; var CIC: TCash_Inflow_Custom);

procedure Cash_OD_Set(var Cash: TCash_OD; const D_RECEIPT,
  D_PAYMENT: TDateTime; const N_DOC: Integer; const AMOUNT: Currency;
  const TYPE_OF_PAYMENT, RECEIVER_ID: Integer;
  const DETAILS_OF_PAYMENT: string; const FIO_ID: Integer; const FIO: TFIO;
  const FIO_KLIENT: string;
  const Address_Free_ID: Integer; const Address: TUM_Address;
  const PAYMENT_DAYS_ID: Integer);
function Cash_Copy(const Payment: TCash_OD): TCash_OD;
procedure CASH_INFLOW_Fields_index(q: TIBQuery;
  var fld: TCash_OD_IndexOfFields); overload;
procedure CASH_INFLOW_Fields_index(q: TIBSQL;
  var fld: TCash_OD_IndexOfFields); overload;

procedure CASH_INFLOW_CUSTOM_Save(TRW: TIBTransaction; var ACIC: TCash_Inflow_Custom);
procedure CASH_INFLOW_Sections_Save(TRW: TIBTransaction; const Cash_inflow:
    TCash_inflow);


// ����� ������� � �������
function Cash_inflow_zak_add(var Payment: TCash_OD;
  const Payment_sum: Currency; const ID_zak: Integer = 0;
  const CASH_INFLOW_ZAK_ID: Integer = 0): Integer;
procedure Cash_inflow_zak_Save(TRW: TIBTransaction; var ACIZ: TCash_inflow_zak;
  const CASH_INFLOW_ID: Integer);

procedure CASHONDELIVERY_ID_ZAK(Transaction: TIBTransaction; iu: TInsUpd;
  const ID_CASHONDELIVERY, ID_zak: Integer);

procedure Save_D_PAY_SEND_UCHMAG(DB: TIBDatabase; const ID_zak: Integer;
  const ADate: TDateTime);

function ID_PAYMENTDAYS(TRR: TIBTransaction; const D_Priem: TDateTime;
  const TYPE_OF_PAYMENT, ID_RECEIVER: Integer): Integer;
function Get_New_ID_PAYMENT_DAYS(TRW: TIBTransaction; const D_Priem: TDateTime;
  const TYPE_OF_PAYMENT, ID_RECEIVER, Status: Integer): Integer;

function PayPostal_Create: PPayPostal;
procedure PayPostal_Free(var Postal: PPayPostal);
procedure PayPostal_Clear(var Postal: TPayPostal);
function PayPostal_Copy(const Postal: PPayPostal): TPayPostal;

function Get_Amount_CashInflow(TRR: TIBTransaction;
  const ID_zak: Integer; const TypeOfPayment_ID: Integer=-1): Currency;

function Payment_ImageIndex(const TYPE_OF_PAYMENT: Integer): Integer;


const
  TOP_NONE          = 0;  // �� ������ ������ ������
  TOP_NalPay        = 1;  // ���������� ������
  TOP_SBERBANK_UCH  = 2;  // �� ����� ��������
  TOP_InterUchmag   = 8;  // ���������� ���� ������
  TOP_Org           = 9;  //��� �����������. ����������� ������
  TOP_Kassa         = 11; // ��������
  TOP_PM            = 12; // Paymaster (������ WebMoney)
  TOP_SBERBANK      = 13; // ��������
  TOP_TERMINAL      = 16; // ��������
  TOP_SBERBANK_KANC = 18; // �� ����� �������� � ������ 2015
  TOP_ALFABANK      = 20; // ����� ����

  ARasch_Schet : array[0..6] of string = ('40702810211000004825',
                                          '40702810211000006690',
                                          '40702810202430000813',
                                          '40702810202300003225',
                                          '40702810311000080053',
                                          '40802810911170100684',
                                          '40802810811170100629');

type
  TPay_System = (psNone,
                 psNalPay=TOP_NalPay,
                 psSberBank=TOP_SBERBANK_UCH,
                 psPaymaster=TOP_PM,
                 psOnHand,
                 psOnPay,
                 psBezNalYur,
                 psTerminal_1c,
                 psBezNal_1c);


  // ����������� �� ������ � Stat_CashOD
  // APay_System_to_TYPE_OF_PAYMENT: array[TPay_System] of Integer = (0,15,13,12,11,10,9,16,17);
  // ��� 15 ������� �������� "���������� ������"
  // ��� 16 �������� � �������� (1�)
  // ��� 17 ����������� ������, ��������� ����� ���������
type
  TTYPE_OF_PAYMENT = record
    TypeOfPayment_ID: Integer;
    Pay_System_ID: Integer;
    Name: string;
    Name_sml: string;
  end;

var
  // ���������� ������ �������� �����, ����������� ���� ��� � ����� �� �����
  // ������ ���������
  ATYPE_OF_PAYMENT: array of TTYPE_OF_PAYMENT;

  // ������� ��� ������ � ATYPE_OF_PAYMENT
procedure Init_TYPE_OF_PAYMENT(TRR: TIBTransaction);
function List_TYPE_OF_PAYMENT_NM(): string;
function TYPE_OF_PAYMENT_to_DynArr(TRR: TIBTransaction): TIntegerDynArray;
function IndexOfDynArr_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): Integer;
function Name_of_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): string;
function NameSML_of_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): string;
function Pay_System_id_of_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): Integer;
function Get_TYPE_OF_PAYMENT_from_Pay_System(TRR: TIBTransaction;
  const Pay_System_ID: Integer): Integer;
function Get_TYPE_OF_PAYMENT_by_Index(const Index: Integer): Integer;
function Get_TYPE_OF_PAYMENT_by_PaySystem(const Pay_System_ID: Integer)
  : Integer;
function StrToType_Of_Payment(const ValueTOP: string): Integer;

function Pay_System(const Pay_System_ID: Integer): TPay_System;

function Get_ID_Order_from_Note(const ANote: string): Integer;

function JSON_SaveCIZ(const CIZ: TCash_inflow_zak;
      const Path_OutUchMag: string; AName,AValue: array of string): string;

procedure CHANGE_CASH_INFLOW_ADD_PAYMENT(TRW: TLibTransaction; var Payment:
    TCash_OD);


implementation

uses SysUtils, DateUtils, CustomInsUpd, uParams, NumJob;

procedure Cash_OD_Zero(var Payment: TCash_OD);
begin
  ZeroMemory(@Payment, SizeOf(TCash_OD));
end;

procedure Cash_OD_Clear(var Payment: TCash_OD);
begin
  if Assigned(Payment.Postal) then
    PayPostal_Free(Payment.Postal);
  UM_Address_Clear(Payment.Cash_inflow.Address);
  SetLength(Payment.Cash_inflow_zak, 0);

  Finalize(Payment.Cash_inflow.FIO);
  Finalize(Payment);
  Cash_OD_Zero(Payment);
end;

procedure Clear_Cash_OD(var Payment: TCash_OD);
begin
  Cash_OD_Clear(Payment);
end;

function ID_PAYMENTDAYS(TRR: TIBTransaction; const D_Priem: TDateTime;
  const TYPE_OF_PAYMENT, ID_RECEIVER: Integer): Integer;
begin
  Result := GetValueFieldInt(TRR,
    'Select ID From PAYMENT_DAYS Where D_DOC=:D_DOC and ' +
      'TYPE_OF_PAYMENT=:TYPE_OF_PAYMENT and ID_RECEIVER=:ID_RECEIVER',
    ['D_DOC', 'TYPE_OF_PAYMENT', 'ID_RECEIVER'], [DateToStr(D_Priem),
    TYPE_OF_PAYMENT, ID_RECEIVER]);
end;

function Get_New_ID_PAYMENT_DAYS(TRW: TIBTransaction; const D_Priem: TDateTime;
  const TYPE_OF_PAYMENT, ID_RECEIVER, Status: Integer): Integer;
var
  iu: TInsUpd;
begin
  Result := GetValueFieldInt(TRW,
    'select KeyOut from MAKE_UNIQUE_GEN(''GEN_PAYMENT_DAYS_ID'')');

  iu := TInsUpd.Create(nil, TRW, kiuInsert, 'PAYMENT_DAYS');
  try
    iu.ValueInt('ID', Result);
    iu.ValueDate('D_DOC', DateOf(D_Priem));
    iu.ValueInt('ID_RECEIVER', ID_RECEIVER);
    iu.ValueInt('TYPE_OF_PAYMENT', TYPE_OF_PAYMENT);
    iu.ValueInt('STATUS', Status);

    iu.Execute;

  finally
    iu.Free;
  end;

end;

procedure CASHONDELIVERY_Save(TRW: TIBTransaction; var Payment: TCash_OD;
  const Links_KIND: Integer; Address: PUM_Address = nil);
var
  ID_zak: Integer;
  iu: TInsUpd;
  // ������� ������
begin
  if Payment.Cash_inflow.PAYMENT_DAYS_ID = 0 then
  begin
    Payment.Cash_inflow.PAYMENT_DAYS_ID := ID_PAYMENTDAYS(TRW,
      Payment.Cash_inflow.D_RECEIPT, Payment.Cash_inflow.TYPE_OF_PAYMENT,
      Payment.Cash_inflow.RECIEVER_ID);
    if Payment.Cash_inflow.PAYMENT_DAYS_ID = 0 then
      Payment.Cash_inflow.PAYMENT_DAYS_ID := Get_New_ID_PAYMENT_DAYS(TRW,
        Payment.Cash_inflow.D_RECEIPT, Payment.Cash_inflow.TYPE_OF_PAYMENT,
        Payment.Cash_inflow.RECIEVER_ID, 0);
  end;

  // ������ ���� 1�. ��� ������ ����, ������� ��������� � D_Priem
  if DateToStr(Payment.Cash_inflow.D_RECEIPT) = '01.01.0100' then
    Payment.Cash_inflow.D_RECEIPT := Payment.Cash_inflow.D_PAYMENT;
  if DateToStr(Payment.Cash_inflow.D_PAYMENT) = '01.01.0100' then
    Payment.Cash_inflow.D_PAYMENT := Payment.Cash_inflow.D_RECEIPT;
{$IFDEF VER150}
  ID_zak := CIZ_ID_ZAK(Payment.Cash_inflow_zak, 0);
{$ELSE}
  ID_zak := Payment.GetCIZ_ID_Zak(0);
{$ENDIF}
  iu := TInsUpd.Create(nil, TRW, kiuInsertUpdate, 'STAT_CASHONDELIVERY');
  try
    if Payment.ID_CASHONDELIVERY = 0 then
    begin
      Payment.ID_CASHONDELIVERY := GetValueFieldInt(TRW,
        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_STAT_CASHONDELIVERY_ID'')');

      if (Links_KIND > 0) and (ID_zak > 0) then
      begin
        iu.knd_IU := kiuInsert;
        iu.TableName := 'Links';
        // IU.AddWhere(
        iu.ValuesInt(['ID_MASTER', 'ID_SECONDARY', 'KIND'],
          [Payment.ID_CASHONDELIVERY, ID_zak, Links_KIND]);
        { Links_KIND ���� 7 }
        iu.Execute;
      end;
    end;
    iu.knd_IU := kiuInsertUpdate;
    iu.TableName := 'STAT_CASHONDELIVERY';
    iu.AddWhere('ID_CASHONDELIVERY', Payment.ID_CASHONDELIVERY);

    iu.ValueDate('D_Priem', Payment.Cash_inflow.D_PAYMENT);
    iu.ValueDate('D_RECEIPT', Payment.Cash_inflow.D_RECEIPT);

    iu.ValueStr('Klient_Fio', Payment.Cash_inflow.FIO_KLIENT);
    iu.ValueInt('N_Priem', Payment.Cash_inflow.N_DOC);
    iu.ValueInt('ID_ZAK', ID_zak); // ???
    iu.ValueInt('ID_RECEIVER', Payment.Cash_inflow.RECIEVER_ID);
    iu.ValueCurr('AMOUNT', Payment.Cash_inflow.AMOUNT);
    iu.ValueStr('DETAILS_OF_PAYMENT', Payment.Cash_inflow.DETAILS_OF_PAYMENT);
    iu.ValueInt('TYPE_OF_PAYMENT', Payment.Cash_inflow.TYPE_OF_PAYMENT);

    if Assigned(Payment.Postal) then
    begin
      iu.ValueStr('INDEX_PRIEM', Payment.Postal.INDEX_PRIEM, True, True);
      iu.ValueStr('RPO', Payment.Postal.RPO, True, True);
      iu.ValueStr('ID_PostalItem', Payment.Postal.INPO.IDC_POSTALITEM);
      iu.ValueStr('KGP_PRIEM', Payment.Postal.KGP_PRIEM, True, True);
      iu.ValueInt('ID_File', Payment.Postal.ID_File);
    end;

    if Assigned(Address) then
    begin
      iu.ValueStr('Klient_Region',
        ListToString([Address.REGION, Address.DISTRICT, Address.LOCALITY], ' ',
          True, True));
      iu.ValueStr('Klient_Address', {$IFDEF VER150}UM_Address_Street(Address^)
                                    {$ELSE}Address.Address_Street{$ENDIF});

    end
    else
    begin
      iu.ValueStr('Klient_Region',
        ListToString([Payment.Cash_inflow.Address.REGION,
          Payment.Cash_inflow.Address.DISTRICT,
          Payment.Cash_inflow.Address.LOCALITY], ' ', True, True));
      iu.ValueStr('Klient_Address',
        {$IFDEF VER150}UM_Address_Street(Payment.Cash_inflow.Address)
        {$ELSE}Payment.Cash_inflow.Address.Address_Street{$ENDIF});
    end;

    iu.Execute;
  finally
    iu.Free;
  end;
end;

procedure Save_D_PAY_SEND_UCHMAG(DB: TIBDatabase; const ID_zak: Integer;
  const ADate: TDateTime);
var
  iu: TInsUpd;
begin
  iu := TInsUpd.Create(nil, DB, kiuUpdate, 'ZAKAZ_FAST');
  try
    iu.AddWhere('ID_ZAK', ID_zak);
    iu.ValueDate('D_PAY_SEND_UCHMAG', ADate);
    iu.Execute;
  finally
    iu.Free;
  end;
end;

function PayPostal_Create: PPayPostal;
begin
  // TODO -cMM: PayPostal_Create default body inserted
  Result := AllocMem(SizeOf(TPayPostal));
end;

procedure PayPostal_Clear(var Postal: TPayPostal);
begin
  // TODO -cMM: PayPostal_Clear default body inserted
  Finalize(Postal);
  ZeroMemory(@Postal, SizeOf(TPayPostal));
end;

procedure PayPostal_Free(var Postal: PPayPostal);
begin
  // TODO -cMM: PayPostal_Free default body inserted
  PayPostal_Clear(Postal^);
  FreeMem(Postal, SizeOf(TPayPostal));
  Postal := nil;
end;

procedure Init_TYPE_OF_PAYMENT(TRR: TIBTransaction);
var
  I: Integer;
  q: TIBQuery;
begin
  if Length(ATYPE_OF_PAYMENT) > 0 then // ������ ������. ������ �� ���� ������
    Exit;
  q := TIBQuery.Create(nil);
  try
    q.Transaction := TRR;
    q.SQL.Text :=
      'select PAY_SYSTEM_NM,PAY_SYSTEM_NM_SML,PAY_SYSTEM_ID,TYPE_OF_PAYMENT From  UM_PAY_SYSTEM ups ORDER by ID';
    q.Open;
    q.FetchAll;
    SetLength(ATYPE_OF_PAYMENT, q.RecordCount);
    for I := 0 to q.RecordCount - 1 do
    begin
      ATYPE_OF_PAYMENT[I].TypeOfPayment_ID := q.FieldByName('TYPE_OF_PAYMENT')
        .AsInteger;
      ATYPE_OF_PAYMENT[I].Pay_System_ID := q.FieldByName('Pay_System_ID')
        .AsInteger;
      ATYPE_OF_PAYMENT[I].Name := q.FieldByName('PAY_SYSTEM_NM').AsString;
      ATYPE_OF_PAYMENT[I].Name_sml := q.FieldByName('PAY_SYSTEM_NM_SML')
        .AsString;
      q.Next;
    end;
  finally
    q.Free;
  end;

end;

function StrToType_Of_Payment(const ValueTOP: string): Integer;
begin
  // TODO -cMM: StrToTYPE_OF_PAYMENT default body inserted
  if AnsiCompareText(ValueTOP, '��������') = 0 then
    Result := TOP_SBERBANK // 2
  else if AnsiCompareText(ValueTOP, '�������� ��� ���������') = 0 then
    Result := TOP_Kassa // 4
  else if AnsiCompareText(ValueTOP, 'WEBMONEY') = 0 then
    Result := TOP_PM // 5
  else if AnsiCompareText(ValueTOP, '��������') = 0 then
    Result := TOP_TERMINAL
  else if AnsiCompareText(ValueTOP, 'ONPAY') = 0 then
    Result := 10
  else if AnsiCompareText(ValueTOP, '����������� ������') = 0 then
    Result := 17
  else
    Result := 0;
end;

function Get_TYPE_OF_PAYMENT_from_Pay_System(TRR: TIBTransaction;
  const Pay_System_ID: Integer): Integer;
begin
  // TODO -cMM: Get_TYPE_OF_PAYMENT_from_Pay_System default body inserted
  Result := GetValueFieldInt(TRR,
    'select ups.TYPE_OF_PAYMENT from UM_PAY_SYSTEM ups where ups.PAY_SYSTEM_ID=:PAY_SYSTEM_ID'
      , ['PAY_SYSTEM_ID'], [Pay_System_ID]); ;
end;

function List_TYPE_OF_PAYMENT_NM(): string;
var
  I: Integer;
begin
  // ������ �������� ����� � ��������� ���� ����������� 1310 ��� �������� � ������
  Result := '';
  for I := 0 to High(ATYPE_OF_PAYMENT) do
  begin
    if I > 0 then
      Result := Result + #13#10;

    Result := Result + ATYPE_OF_PAYMENT[I].Name
  end;
end;

function TYPE_OF_PAYMENT_to_DynArr(TRR: TIBTransaction): TIntegerDynArray;
begin
  // � ������ ����� ������� �������� �� ������ TYPE_OF_PAYMENT
  StringToIntArray(GetTextFromSQL(TRR,
      'select TYPE_OF_PAYMENT From UM_PAY_SYSTEM ORDER by ID', [], [], ','),
    Result);
end;

function IndexOfDynArr_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): Integer;
var
  I: Integer;
begin
  // TODO -cMM: IndexOfDynArr_TYPE_OF_PAYMENT default body inserted
  Result := -1;
  for I := 0 to High(ATYPE_OF_PAYMENT) do
    if ATYPE_OF_PAYMENT[I].TypeOfPayment_ID = TYPE_OF_PAYMENT then
    begin
      Result := I;
      Exit;
    end;
end;

function Name_of_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): string;
begin
  // ������� �������� ������� ������
  Result := ATYPE_OF_PAYMENT[IndexOfDynArr_TYPE_OF_PAYMENT(TYPE_OF_PAYMENT)]
    .Name;
end;

function Get_TYPE_OF_PAYMENT_by_Index(const Index: Integer): Integer;
begin
  // �������� �������� TYPE_OF_PAYMENT �� ������� � �������
  Result := ATYPE_OF_PAYMENT[Index].TypeOfPayment_ID;
end;

function NameSML_of_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): string;
begin
  // ������� ������� �������� ������� ������
  Result := ATYPE_OF_PAYMENT[IndexOfDynArr_TYPE_OF_PAYMENT(TYPE_OF_PAYMENT)]
    .Name_sml;
end;

function Pay_System_id_of_TYPE_OF_PAYMENT(const TYPE_OF_PAYMENT: Integer): Integer;
begin
  //
  Result := ATYPE_OF_PAYMENT[IndexOfDynArr_TYPE_OF_PAYMENT(TYPE_OF_PAYMENT)]
    .Pay_System_ID;
end;

function Get_TYPE_OF_PAYMENT_by_PaySystem(const Pay_System_ID: Integer)
  : Integer;
var
  I: Integer;
begin
  // TODO -cMM: Get_TYPE_OF_PAYMENT_by_PaySystem default body inserted
  Result := 0;
  for I := 0 to High(ATYPE_OF_PAYMENT) do
    if ATYPE_OF_PAYMENT[I].Pay_System_ID = Pay_System_ID then
    begin
      Result := ATYPE_OF_PAYMENT[I].TypeOfPayment_ID;
      Exit;
    end;
end;

function Pay_System(const Pay_System_ID: Integer): TPay_System;
begin
  if (Pay_System_ID >= 1) and (Pay_System_ID <= Ord( High(TPay_System))) then
    Result := TPay_System(Pay_System_ID)
  else
    Result := psNone;
end;

function Get_Amount_CashInflow(TRR: TIBTransaction;
  const ID_zak: Integer; const TypeOfPayment_ID: Integer=-1): Currency;
begin
  if TypeOfPayment_ID=-1 then
    Result := GetValueFieldCurr(TRR,
      'Select sum(ciz.Payment_sum) from CASH_INFLOW_ZAK ciz ' +
        'where (ciz.ID_ZAK=:ID_ZAK) and ciz.SENT_TO_PERSONAL_ACCOUNT=0', ['ID_ZAK'], [ID_zak])
  else
    Result := GetValueFieldCurr(TRR,
      'Select sum(ciz.Payment_sum) from CASH_INFLOW_ZAK ciz ' +
        'join CASH_INFLOW ci on ciz.CASH_INFLOW_ID=ci.CASH_INFLOW_ID ' +
        'where (ciz.ID_ZAK=:ID_ZAK) and ci.TYPE_OF_PAYMENT=:TYPE_OF_PAYMENT and ciz.SENT_TO_PERSONAL_ACCOUNT=0',
        ['ID_ZAK','TYPE_OF_PAYMENT'], [ID_zak, TypeOfPayment_ID]);
end;

procedure CASHONDELIVERY_ID_ZAK(Transaction: TIBTransaction; iu: TInsUpd;
  const ID_CASHONDELIVERY, ID_zak: Integer);
var
  NeedFree: Boolean;
begin
  // TODO -cMM: CASHONDELIVERY_ID_ZAK default body inserted
  NeedFree := (iu = nil);
  if NeedFree then
    iu := TInsUpd.Create(nil, Transaction, kiuUpdate, 'STAT_CASHONDELIVERY')
  else
  begin
    iu.TableName := 'STAT_CASHONDELIVERY';
    iu.knd_IU := kiuUpdate;
  end;
  try
    iu.AddWhere('ID_CASHONDELIVERY', ID_CASHONDELIVERY);
    iu.ValueInt('ID_ZAK', ID_zak);
    iu.Execute;
  finally
    if NeedFree then
      iu.Free;
  end;
end;

procedure Cash_OD_Set(var Cash: TCash_OD; const D_RECEIPT,
  D_PAYMENT: TDateTime; const N_DOC: Integer; const AMOUNT: Currency;
  const TYPE_OF_PAYMENT, RECEIVER_ID: Integer;
  const DETAILS_OF_PAYMENT: string; const FIO_ID: Integer; const FIO: TFIO;
  const FIO_KLIENT: string;
  const Address_Free_ID: Integer; const Address: TUM_Address;
  const PAYMENT_DAYS_ID: Integer);
begin
  // ��������� ������
  // ���� ����������� � �������
  Cash.Cash_inflow.D_RECEIPT := DateOf(D_RECEIPT);
  // ���� ������ �� �������
  Cash.Cash_inflow.D_PAYMENT := D_PAYMENT;
  // ���� ���� ������� �������� �� �������, �� ��������� ���� ����� � ��� ���������
  if (Cash.Cash_inflow.D_PAYMENT = 0) or
    (Cash.Cash_inflow.D_PAYMENT = StrToDate('01.01.0100')) then
    Cash.Cash_inflow.D_PAYMENT := D_RECEIPT;
  // Cash.D_Priem1                := Cash.Cash_income.D_PAYMENT;
  // ����� ������ � �������
  Cash.Cash_inflow.N_DOC := N_DOC;
  // Cash.N_Priem     := Cash.Cash_income.N_DOC;
  // ����� ������
  Cash.Cash_inflow.AMOUNT := AMOUNT;
  // Cash.Amount                      := Cash.Cash_income.AMOUNT;
  // cash_income_Zak_add(Cash, AMOUNT, ID_ZAK, 0);
  // ��� �������
  Cash.Cash_inflow.TYPE_OF_PAYMENT := TYPE_OF_PAYMENT;
  // ��� ����� �����������
  Cash.Cash_inflow.RECIEVER_ID := RECEIVER_ID;
  // ������ �������
  Cash.Cash_inflow.DETAILS_OF_PAYMENT := Trim(DETAILS_OF_PAYMENT);
  // ��� �����������
  Cash.Cash_inflow.FIO := FIO;
  Cash.Cash_inflow.FIO_KLIENT := FIO_KLIENT;
  Cash.Cash_inflow.FIO_ID := FIO_ID;
  // � ������ ������ ���������
  // Cash.Cash_income_zak.ID_ZAK := ID_ZAK;
  // Cash.ID_ZAK := Cash.Cash_income_zak.ID_ZAK;
  // ������������� ���
  Cash.Cash_inflow.PAYMENT_DAYS_ID := PAYMENT_DAYS_ID;
  // Cash.ID_PAYMENT_DAYS := Cash.Cash_income.PAYMENT_DAYS_ID;
  // �����
  Cash.Cash_inflow.Address := Address;
  // ����������� ���������� ID_Address.
  // Address ����� ��������� �� ������� ������ � ����� � ����� �� ������ ID
  Cash.Cash_inflow.Address.ID_Address := Address_Free_ID;
  // Cash.Cash_income_zak.CASH_INFLOW_ZAK_ID := 0;
end;

procedure CASH_INFLOW_Fields_index(q: TIBQuery;
  var fld: TCash_OD_IndexOfFields);
begin
  ZeroMemory(@fld, SizeOf(TCash_OD_IndexOfFields));
  fld.active := True;
  fld.D_RECEIPT := q.Fields.FindField('D_RECEIPT').Index;
  fld.ID_RECEIVER := q.Fields.FindField('ID_RECEIVER').Index;
  fld.TYPE_OF_PAYMENT := q.Fields.FindField('TYPE_OF_PAYMENT').Index;
  fld.Status := q.Fields.FindField('Status').Index;
  fld.ID_PAYMENT_DAYS := q.Fields.FindField('ID_PAYMENT_DAYS').Index;
  fld.NM_RECEIVER := q.Fields.FindField('NM_RECEIVER').Index;
  fld.ID_CASHONDELIVERY := q.Fields.FindField('ID_CASHONDELIVERY').Index;
  fld.D_Priem := q.Fields.FindField('D_Priem').Index;
  fld.N_Priem := q.Fields.FindField('N_Priem').Index;
  fld.AMOUNT := q.Fields.FindField('Amount').Index;
  fld.KLIENT_FIO := q.Fields.FindField('KLIENT_FIO').Index;
  fld.DETAILS_OF_PAYMENT := q.Fields.FindField('DETAILS_OF_PAYMENT').Index;
  fld.ID_zak := q.Fields.FindField('ID_ZAK').Index;
  if Assigned(q.Fields.FindField('N_ZAK')) then
    fld.N_Zak := q.Fields.FindField('N_ZAK').Index
  else
    fld.N_Zak := -1;
  if Assigned(q.Fields.FindField('NP_ZAK')) then
    fld.NP_ZAK := q.Fields.FindField('NP_ZAK').Index
  else
    fld.NP_ZAK := -1;
  if Assigned(q.Fields.FindField('NC_ZAK')) then
    fld.NC_ZAK := q.Fields.FindField('NC_ZAK').Index
  else
    fld.NC_ZAK := -1;
  fld.D_Zak := q.Fields.FindField('CIZ_D_Zak').Index;
  if Assigned(q.Fields.FindField('NMKLIENT')) then
    fld.NMKLIENT := q.Fields.FindField('NMKLIENT').Index
  else
    fld.NMKLIENT := -1;
  if Assigned(q.Fields.FindField('ID_Canceled')) then
    fld.ID_Canceled := q.Fields.FindField('ID_Canceled').Index
  else
    fld.ID_Canceled := -1;
  fld.SEND_READINESS := q.Fields.FindField('SEND_READINESS').Index;
  if Assigned(q.Fields.FindField('WO_ORDER')) then
    fld.WO_ORDER := q.Fields.FindField('WO_ORDER').Index
  else
    fld.WO_ORDER := -1;
  if Assigned(q.Fields.FindField('ID_DELIVERY')) then
    fld.ID_DELIVERY := q.Fields.FindField('ID_DELIVERY').Index
  else
    fld.ID_DELIVERY := -1;
  fld.CASH_INFLOW_ID := q.Fields.FindField('CASH_INFLOW_ID').Index;
  if Assigned(q.Fields.FindField('CI_FIO_ID')) then
    fld.CI_FIO_ID := q.Fields.FindField('CI_FIO_ID').Index
  else
    fld.CI_FIO_ID := -1;
  if Assigned(q.Fields.FindField('CI_Fio_Klient')) then
    fld.CI_Fio_Klient := q.Fields.FindField('CI_Fio_Klient').Index
  else
    fld.CI_Fio_Klient := -1;
//  if Assigned(q.Fields.FindField('Fio_Klient')) then
//    fld.FIO_KLIENT := q.Fields.FindField('Fio_Klient').Index
//  else
//    fld.FIO_KLIENT := -1;
  if Assigned(q.Fields.FindField('Fio_Surname')) then
    fld.Fio_Surname := q.Fields.FindField('Fio_Surname').Index
  else
    fld.Fio_Surname := -1;
  if Assigned(q.Fields.FindField('Fio_Name')) then
    fld.Fio_Name := q.Fields.FindField('Fio_Name').Index
  else
    fld.Fio_Name := -1;
  if Assigned(q.Fields.FindField('Fio_Patronymic')) then
    fld.Fio_Patronymic := q.Fields.FindField('Fio_Patronymic').Index
  else
    fld.Fio_Patronymic := -1;
  if Assigned(q.Fields.FindField('CI_ADDRESS_FREE_ID')) then
    fld.CI_ADDRESS_FREE_ID := q.Fields.FindField('CI_ADDRESS_FREE_ID').Index
  else
    fld.CI_ADDRESS_FREE_ID := -1;
  if Assigned(q.Fields.FindField('POST_Index_Priem')) then
    fld.POST_Index_Priem := q.Fields.FindField('POST_Index_Priem').Index
  else
    fld.POST_Index_Priem := -1;
  if Assigned(q.Fields.FindField('Klient_Region')) then
    fld.Klient_Region := q.Fields.FindField('Klient_Region').Index
  else
    fld.Klient_Region := -1;
  if Assigned(q.Fields.FindField('Klient_Address')) then
    fld.Klient_Address := q.Fields.FindField('Klient_Address').Index
  else
    fld.Klient_Address := -1;
  if Assigned(q.Fields.FindField('KAF_ZIP_CODE')) then
    fld.KAF_ZIP_CODE := q.Fields.FindField('KAF_ZIP_CODE').Index
  else
    fld.KAF_ZIP_CODE := -1;
  if Assigned(q.Fields.FindField('KAF_REGION')) then
    fld.KAF_REGION := q.Fields.FindField('KAF_REGION').Index
  else
    fld.KAF_REGION := -1;
  if Assigned(q.Fields.FindField('KAF_ADDRESS')) then
    fld.KAF_ADDRESS := q.Fields.FindField('KAF_ADDRESS').Index
  else
    fld.KAF_ADDRESS := -1;
  if Assigned(q.Fields.FindField('KAF_ADDRESS_FREE')) then
    fld.KAF_ADDRESS_FREE := q.Fields.FindField('KAF_ADDRESS_FREE').Index
  else
    fld.KAF_ADDRESS_FREE := -1;
  if Assigned(q.Fields.FindField('CI_AMOUNT')) then
    fld.CI_AMOUNT := q.Fields.FindField('CI_AMOUNT').Index
  else
    fld.CI_AMOUNT := -1;
  if Assigned(q.Fields.FindField('CI_D_RECEIPT')) then
    fld.CI_D_RECEIPT := q.Fields.FindField('CI_D_RECEIPT').Index
  else
    fld.CI_D_RECEIPT := -1;
  if Assigned(q.Fields.FindField('CI_D_PAYMENT')) then
    fld.CI_D_PAYMENT := q.Fields.FindField('CI_D_PAYMENT').Index
  else
    fld.CI_D_PAYMENT := -1;
  if Assigned(q.Fields.FindField('CI_N_DOC')) then
    fld.CI_N_DOC := q.Fields.FindField('CI_N_DOC').Index
  else
    fld.CI_N_DOC := -1;
//  if Assigned(q.Fields.FindField('CI_TYPE_OF_PAYMENT')) then
//    fld.CI_TYPE_OF_PAYMENT := q.Fields.FindField('CI_TYPE_OF_PAYMENT').Index
//  else
//    fld.CI_TYPE_OF_PAYMENT := -1;
  if Assigned(q.Fields.FindField('CI_RECIEVER_ID')) then
    fld.CI_RECIEVER_ID := q.Fields.FindField('CI_RECIEVER_ID').Index
  else
    fld.CI_RECIEVER_ID := -1;
  if Assigned(q.Fields.FindField('CI_DETAILS_OF_PAYMENT')) then
    fld.CI_DETAILS_OF_PAYMENT := q.Fields.FindField('CI_DETAILS_OF_PAYMENT')
      .Index
  else
    fld.CI_DETAILS_OF_PAYMENT := -1;
  if Assigned(q.Fields.FindField('CI_PAYMENT_DAYS_ID')) then
    fld.CI_PAYMENT_DAYS_ID := q.Fields.FindField('CI_PAYMENT_DAYS_ID').Index
  else
    fld.CI_PAYMENT_DAYS_ID := -1;

  if Assigned(q.Fields.FindField('CI_PAYER_INFO_ID')) then
    fld.CI_PAYER_INFO_ID := q.Fields.FindField('CI_PAYER_INFO_ID').Index
  else
    fld.CI_PAYER_INFO_ID := -1;

  if Assigned(q.Fields.FindField('PAY_SYSTEM_NM_SML')) then
    fld.PAY_SYSTEM_NM_SML := q.Fields.FindField('PAY_SYSTEM_NM_SML').Index
  else
    fld.PAY_SYSTEM_NM_SML := -1;
  if Assigned(q.Fields.FindField('CASH_INFLOW_ZAK_ID')) then
    fld.CASH_INFLOW_ZAK_ID := q.Fields.FindField('CASH_INFLOW_ZAK_ID').Index
  else
    fld.CASH_INFLOW_ZAK_ID := -1;
  if Assigned(q.Fields.FindField('CIZ_Payment_sum')) then
    fld.CIZ_Payment_sum := q.Fields.FindField('CIZ_Payment_sum').Index
  else
    fld.CIZ_Payment_sum := -1;
  if Assigned(q.Fields.FindField('CIZ_ID_ZAK')) then
    fld.CIZ_ID_ZAK := q.Fields.FindField('CIZ_ID_ZAK').Index
  else
    fld.CIZ_ID_ZAK := -1;
  if Assigned(q.Fields.FindField('CIZ_N_ZAK')) then
    fld.CIZ_N_ZAK := q.Fields.FindField('CIZ_N_ZAK').Index
  else
    fld.CIZ_N_ZAK := -1;
  if Assigned(q.Fields.FindField('CIZ_D_Zak')) then
    fld.CIZ_D_Zak := q.Fields.FindField('CIZ_D_Zak').Index
  else
    fld.CIZ_D_Zak := -1;

  if Assigned(q.Fields.FindField('SENT_TO_PERSONAL_ACCOUNT')) then
    fld.CIZ_SENT_TO_PERSONAL_ACCOUNT := q.Fields.FindField('SENT_TO_PERSONAL_ACCOUNT').Index
  else
    fld.CIZ_D_Zak := -1;

  if Assigned(q.Fields.FindField('POST_IDC_POSTALITEM')) then
    fld.POST_IDC_POSTALITEM := q.Fields.FindField('POST_IDC_POSTALITEM').Index
  else
    fld.POST_IDC_POSTALITEM := -1;
  if Assigned(q.Fields.FindField('POST_KGP_PRIEM')) then
    fld.POST_KGP_PRIEM := q.Fields.FindField('POST_KGP_PRIEM').Index
  else
    fld.POST_KGP_PRIEM := -1;

  (*
    if Assigned(q.Fields.FindField('')) then
    fld.       := q.Fields.FindField('').Index
    else
    fld.       := -1;

    *)
end;

procedure CASH_INFLOW_Fields_index(q: TIBSQL;
  var fld: TCash_OD_IndexOfFields);
begin
  ZeroMemory(@fld, SizeOf(TCash_OD_IndexOfFields));
  fld.active := True;
  fld.D_RECEIPT := q.FieldIndex['D_RECEIPT'];
  fld.ID_RECEIVER := q.FieldIndex['ID_RECEIVER'];
  fld.TYPE_OF_PAYMENT := q.FieldIndex['TYPE_OF_PAYMENT'];
  fld.Status := q.FieldIndex['Status'];
  fld.ID_PAYMENT_DAYS := q.FieldIndex['ID_PAYMENT_DAYS'];
  fld.NM_RECEIVER := q.FieldIndex['NM_RECEIVER'];
  fld.ID_CASHONDELIVERY := q.FieldIndex['ID_CASHONDELIVERY'];
  fld.D_Priem := q.FieldIndex['D_Priem'];
  fld.N_Priem := q.FieldIndex['N_Priem'];
  fld.AMOUNT := q.FieldIndex['Amount'];
  fld.KLIENT_FIO := q.FieldIndex['KLIENT_FIO'];
  fld.DETAILS_OF_PAYMENT := q.FieldIndex['DETAILS_OF_PAYMENT'];
  fld.ID_zak := q.FieldIndex['ID_ZAK'];
  fld.N_Zak := q.FieldIndex['N_ZAK'];
  fld.NP_ZAK := q.FieldIndex['NP_ZAK'];
  fld.NC_ZAK := q.FieldIndex['NC_ZAK'];
  fld.D_Zak := q.FieldIndex['CIZ_D_Zak'];
  fld.NMKLIENT := q.FieldIndex['NMKLIENT'];
  fld.ID_Canceled := q.FieldIndex['ID_Canceled'];
  fld.SEND_READINESS := q.FieldIndex['SEND_READINESS'];
  fld.WO_ORDER := q.FieldIndex['WO_ORDER'];
  fld.ID_DELIVERY := q.FieldIndex['ID_DELIVERY'];
  fld.CASH_INFLOW_ID := q.FieldIndex['CASH_INFLOW_ID'];
  fld.CI_FIO_ID := q.FieldIndex['CI_FIO_ID'];
  fld.CI_Fio_Klient := q.FieldIndex['CI_Fio_Klient'];
  fld.Fio_Surname := q.FieldIndex['Fio_Surname'];
  fld.Fio_Name := q.FieldIndex['Fio_Name'];
  fld.Fio_Patronymic := q.FieldIndex['Fio_Patronymic'];
  fld.CI_ADDRESS_FREE_ID := q.FieldIndex['CI_ADDRESS_FREE_ID'];
  fld.POST_Index_Priem := q.FieldIndex['POST_Index_Priem'];
  fld.Klient_Region := q.FieldIndex['Klient_Region'];
  fld.Klient_Address := q.FieldIndex['Klient_Address'];
  fld.KAF_ZIP_CODE := q.FieldIndex['KAF_ZIP_CODE'];
  fld.KAF_REGION := q.FieldIndex['KAF_REGION'];
  fld.KAF_ADDRESS := q.FieldIndex['KAF_ADDRESS'];
  fld.KAF_ADDRESS_FREE := q.FieldIndex['KAF_ADDRESS_FREE'];
  fld.CI_AMOUNT := q.FieldIndex['CI_AMOUNT'];
  fld.CI_D_RECEIPT := q.FieldIndex['CI_D_RECEIPT'];
  fld.CI_D_PAYMENT := q.FieldIndex['CI_D_PAYMENT'];
  fld.CI_N_DOC := q.FieldIndex['CI_N_DOC'];
  fld.CI_RECIEVER_ID := q.FieldIndex['CI_RECIEVER_ID'];
  fld.CI_DETAILS_OF_PAYMENT := q.FieldIndex['CI_DETAILS_OF_PAYMENT'];
  fld.CI_PAYMENT_DAYS_ID := q.FieldIndex['CI_PAYMENT_DAYS_ID'];

  fld.CI_PAYER_INFO_ID := q.FieldIndex['CI_PAYER_INFO_ID'];

  fld.PAY_SYSTEM_NM_SML := q.FieldIndex['PAY_SYSTEM_NM_SML'];
  fld.CASH_INFLOW_ZAK_ID := q.FieldIndex['CASH_INFLOW_ZAK_ID'];
  fld.CIZ_Payment_sum := q.FieldIndex['CIZ_Payment_sum'];
  fld.CIZ_ID_ZAK := q.FieldIndex['CIZ_ID_ZAK'];
  fld.CIZ_N_ZAK := q.FieldIndex['CIZ_N_ZAK'];
  fld.CIZ_D_Zak := q.FieldIndex['CIZ_D_Zak'];
  fld.CIZ_SENT_TO_PERSONAL_ACCOUNT := q.FieldIndex['SENT_TO_PERSONAL_ACCOUNT'];

  fld.POST_IDC_POSTALITEM := q.FieldIndex['POST_IDC_POSTALITEM'];
  fld.POST_KGP_PRIEM := q.FieldIndex['POST_KGP_PRIEM'];
end;

procedure CASH_INFLOW_Load(var Payment: TCash_OD; q: TIBQuery;
  const fld: TCash_OD_IndexOfFields; const PayPostal_Load, Load_CI: Boolean);
var
  CIZ_Index: Integer;
  CI_ID: Integer;
begin
  // ��������� ������ �� �������
  CI_ID := q.Fields[fld.CASH_INFLOW_ID].AsInteger;

  if Load_CI then
  begin
    Payment.Cash_inflow.FIO_ID := q.Fields[fld.CI_FIO_ID].AsInteger;

    if (Payment.Cash_inflow.FIO_ID = 0) and
      (q.Fields[fld.CI_Fio_Klient].AsString <> '') then
      Payment.Cash_inflow.FIO := FIO_Triple
        (q.Fields[fld.CI_Fio_Klient].AsString)
    else if (Payment.Cash_inflow.FIO_ID = 0) and
      (q.Fields[fld.KLIENT_FIO].AsString <> '') then
      Payment.Cash_inflow.FIO := FIO_Triple(q.Fields[fld.KLIENT_FIO].AsString)
    else if (q.Fields[fld.Fio_Surname].AsString <> '') then
      Payment.Cash_inflow.FIO := FIO_Total(q.Fields[fld.Fio_Surname].AsString,
        q.Fields[fld.Fio_Name].AsString,
        q.Fields[fld.Fio_Patronymic].AsString);
    Payment.Cash_inflow.FIO_KLIENT := q.FieldByName('CI_FIO_KLIENT').AsString;

    if (Length(Payment.Cash_inflow.FIO.Surname) >= 50) or
      (Length(Payment.Cash_inflow.FIO.Name) >= 50) or
      (Length(Payment.Cash_inflow.FIO.Patronymic) >= 50) then
    begin
//      Payment.Cash_inflow.FIO_KLIENT := Payment.Cash_inflow.FIO.Total;
      Finalize(Payment.Cash_inflow.FIO);
      Payment.Cash_inflow.FIO_ID := 0;
    end;
    AddressLoad(q, Payment.Cash_inflow.Address);

    // �������������� ��������� Amount=CI_Amount
    if (CI_ID > 0) and (q.Fields[fld.AMOUNT].AsCurrency <> q.Fields
        [fld.CI_AMOUNT].AsCurrency) then
      // ������
      ;

    // SET
    Cash_OD_Set(Payment,
      IfThenVar(CI_ID = 0, q.Fields[fld.D_RECEIPT].AsDateTime,
        q.Fields[fld.CI_D_RECEIPT].AsDateTime),
      IfThenVar(CI_ID = 0, q.Fields[fld.D_Priem].AsDateTime,
        q.Fields[fld.CI_D_PAYMENT].AsDateTime),
      IfThen(CI_ID = 0, q.Fields[fld.N_Priem].AsInteger,
        q.Fields[fld.CI_N_DOC].AsInteger),
      IfThenVar(CI_ID = 0, q.Fields[fld.AMOUNT].AsCurrency,
        q.Fields[fld.CI_AMOUNT].AsCurrency),
      q.Fields[fld.TYPE_OF_PAYMENT].AsInteger,
      IfThen(CI_ID = 0, q.Fields[fld.ID_RECEIVER].AsInteger,
        q.Fields[fld.CI_RECIEVER_ID].AsInteger),
      IfThen(CI_ID = 0, q.Fields[fld.DETAILS_OF_PAYMENT].AsString,
        q.Fields[fld.CI_DETAILS_OF_PAYMENT].AsString),
      q.Fields[fld.CI_FIO_ID].AsInteger, Payment.Cash_inflow.FIO,
      Payment.Cash_inflow.FIO_KLIENT,
      Payment.Cash_inflow.Address.ID_Address, Payment.Cash_inflow.Address,
      IfThen(CI_ID = 0, q.Fields[fld.ID_PAYMENT_DAYS].AsInteger,
        q.Fields[fld.CI_PAYMENT_DAYS_ID].AsInteger));
    Payment.Cash_inflow.CASH_INFLOW_ID := CI_ID;

    Payment.Cash_inflow.PAYMENT_DAYS_ID :=
      q.Fields[fld.CI_PAYMENT_DAYS_ID].AsInteger;
    Payment.Cash_inflow.TYPE_OF_PAYMENT_nm_sml :=
      q.Fields[fld.PAY_SYSTEM_NM_SML].AsString;

    if fld.CI_PAYER_INFO_ID >=0 then
      begin
        Payment.Cash_inflow.Contragent.CASH_INFLOW_CUSTOM_ID :=
          q.Fields[fld.CI_PAYER_INFO_ID].AsInteger;

        StringToIntArray(q.FieldByName('CI_Sections').AsString, Payment.Cash_inflow.Sections);

//        Payment.Cash_inflow.Section:= q.FieldByName('CI_Section').AsInteger;
        Payment.Cash_inflow.Closed:= q.FieldByName('CI_Closed').AsInteger=1;
      end;

  end;
  // ��� ��������� �������� ������ �� �������� �� �����
{$IFDEF VER150}
  CIZ_Index := IndexCIZ_By_Id(Payment.Cash_inflow_zak,
    q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger);
{$ELSE}
  CIZ_Index := Payment.IndexCIZ_By_Id
    (q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger);
{$ENDIF}
  if CIZ_Index = -1 then
    CIZ_Index := Cash_inflow_zak_add(Payment,
      IfThen(q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger = 0,
        Payment.Cash_inflow.AMOUNT, q.Fields[fld.CIZ_Payment_sum].AsCurrency),
      IfThen(q.Fields[fld.CIZ_ID_ZAK].AsInteger = 0,
        q.Fields[fld.ID_zak].AsInteger, q.Fields[fld.CIZ_ID_ZAK].AsInteger),
      q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger);
  Payment.Cash_inflow_zak[CIZ_Index].N_Zak := q.Fields[fld.CIZ_N_ZAK].AsString;
  Payment.Cash_inflow_zak[CIZ_Index].D_Zak :=
    q.Fields[fld.CIZ_D_Zak].AsDateTime;
  Payment.Cash_inflow_zak[CIZ_Index].FIO_KLIENT := FIO_Total
    (q.Fields[fld.FIO_SURNAME].AsString,
    q.Fields[fld.FIO_NAME].AsString,
    q.Fields[fld.FIO_PATRONYMIC].AsString).Total;
  Payment.Cash_inflow_zak[CIZ_Index].Canceled := q.Fields[fld.ID_Canceled].AsInteger>0;
  Payment.Cash_inflow_zak[CIZ_Index].Sent_to_personal_Account :=
    q.Fields[fld.CIZ_SENT_TO_PERSONAL_ACCOUNT].AsInteger = 1;

  Payment.ID_CASHONDELIVERY := q.Fields[fld.ID_CASHONDELIVERY].AsInteger;
  // Payment.TYPE_OF_PAYMENT := q.Fields[fld.TYPE_OF_PAYMENT].AsInteger;

  if PayPostal_Load then
  begin
    if Payment.Postal = nil then
      Payment.Postal := PayPostal_Create()
    else
      PayPostal_Clear(Payment.Postal^);

    Payment.Postal.INDEX_PRIEM := q.Fields[fld.POST_Index_Priem].AsString;
    Payment.Postal.INPO.IDC_POSTALITEM := q.Fields[fld.POST_IDC_POSTALITEM].AsString;
    Payment.Postal.KGP_PRIEM := q.Fields[fld.POST_KGP_PRIEM].AsString;
  end;

end;

procedure CASH_INFLOW_Load(var Payment: TCash_OD; q: TIBSQL;
  const fld: TCash_OD_IndexOfFields; const PayPostal_Load, Load_CI: Boolean);
var
  CIZ_Index: Integer;
  CI_ID: Integer;
begin
  // ��������� ������ �� �������
  CI_ID := q.Fields[fld.CASH_INFLOW_ID].AsInteger;

  if Load_CI then
  begin
    Payment.Cash_inflow.FIO_ID := q.Fields[fld.CI_FIO_ID].AsInteger;

    if (Payment.Cash_inflow.FIO_ID = 0) and
      (q.Fields[fld.CI_Fio_Klient].AsString <> '') then
      Payment.Cash_inflow.FIO := FIO_Triple
        (q.Fields[fld.CI_Fio_Klient].AsString)
    else if (Payment.Cash_inflow.FIO_ID = 0) and
      (q.Fields[fld.KLIENT_FIO].AsString <> '') then
      Payment.Cash_inflow.FIO := FIO_Triple(q.Fields[fld.KLIENT_FIO].AsString)
    else if (q.Fields[fld.Fio_Surname].AsString <> '') then
      Payment.Cash_inflow.FIO := FIO_Total(q.Fields[fld.Fio_Surname].AsString,
        q.Fields[fld.Fio_Name].AsString,
        q.Fields[fld.Fio_Patronymic].AsString);
    Payment.Cash_inflow.FIO_KLIENT := q.FieldByName('CI_FIO_KLIENT').AsString;

    if (Length(Payment.Cash_inflow.FIO.Surname) >= 50) or
      (Length(Payment.Cash_inflow.FIO.Name) >= 50) or
      (Length(Payment.Cash_inflow.FIO.Patronymic) >= 50) then
    begin
//      Payment.Cash_inflow.FIO_KLIENT := Payment.Cash_inflow.FIO.Total;
      Finalize(Payment.Cash_inflow.FIO);
      Payment.Cash_inflow.FIO_ID := 0;
    end;
    AddressLoad(q, Payment.Cash_inflow.Address);

    // �������������� ��������� Amount=CI_Amount
    if (CI_ID > 0) and (q.Fields[fld.AMOUNT].AsCurrency <> q.Fields
        [fld.CI_AMOUNT].AsCurrency) then
      // ������
      ;

    // SET
    Cash_OD_Set(Payment,
      IfThenVar(CI_ID = 0, q.Fields[fld.D_RECEIPT].AsDateTime,
        q.Fields[fld.CI_D_RECEIPT].AsDateTime),
      IfThenVar(CI_ID = 0, q.Fields[fld.D_Priem].AsDateTime,
        q.Fields[fld.CI_D_PAYMENT].AsDateTime),
      IfThen(CI_ID = 0, q.Fields[fld.N_Priem].AsInteger,
        q.Fields[fld.CI_N_DOC].AsInteger),
      IfThenVar(CI_ID = 0, q.Fields[fld.AMOUNT].AsCurrency,
        q.Fields[fld.CI_AMOUNT].AsCurrency),
      q.Fields[fld.TYPE_OF_PAYMENT].AsInteger,
      IfThen(CI_ID = 0, q.Fields[fld.ID_RECEIVER].AsInteger,
        q.Fields[fld.CI_RECIEVER_ID].AsInteger),
      IfThen(CI_ID = 0, q.Fields[fld.DETAILS_OF_PAYMENT].AsString,
        q.Fields[fld.CI_DETAILS_OF_PAYMENT].AsString),
      q.Fields[fld.CI_FIO_ID].AsInteger, Payment.Cash_inflow.FIO,
      Payment.Cash_inflow.FIO_KLIENT,
      Payment.Cash_inflow.Address.ID_Address, Payment.Cash_inflow.Address,
      IfThen(CI_ID = 0, q.Fields[fld.ID_PAYMENT_DAYS].AsInteger,
        q.Fields[fld.CI_PAYMENT_DAYS_ID].AsInteger));
    Payment.Cash_inflow.CASH_INFLOW_ID := CI_ID;

    Payment.Cash_inflow.PAYMENT_DAYS_ID :=
      q.Fields[fld.CI_PAYMENT_DAYS_ID].AsInteger;
    Payment.Cash_inflow.TYPE_OF_PAYMENT_nm_sml :=
      q.Fields[fld.PAY_SYSTEM_NM_SML].AsString;

    if fld.CI_PAYER_INFO_ID >=0 then
      begin
        Payment.Cash_inflow.Contragent.CASH_INFLOW_CUSTOM_ID :=
          q.Fields[fld.CI_PAYER_INFO_ID].AsInteger;

        StringToIntArray(q.FieldByName('CI_Sections').AsString, Payment.Cash_inflow.Sections);
//        Payment.Cash_inflow.Section:= q.FieldByName('CI_Section').AsInteger;
        Payment.Cash_inflow.Closed:= q.FieldByName('CI_Closed').AsInteger=1;
      end;

  end;
  // ��� ��������� �������� ������ �� �������� �� �����
{$IFDEF VER150}
  CIZ_Index := IndexCIZ_By_Id(Payment.Cash_inflow_zak,
    q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger);
{$ELSE}
  CIZ_Index := Payment.IndexCIZ_By_Id
    (q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger);
{$ENDIF}
  if CIZ_Index = -1 then
    CIZ_Index := Cash_inflow_zak_add(Payment,
      IfThen(q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger = 0,
        Payment.Cash_inflow.AMOUNT, q.Fields[fld.CIZ_Payment_sum].AsCurrency),
      IfThen(q.Fields[fld.CIZ_ID_ZAK].AsInteger = 0,
        q.Fields[fld.ID_zak].AsInteger, q.Fields[fld.CIZ_ID_ZAK].AsInteger),
      q.Fields[fld.CASH_INFLOW_ZAK_ID].AsInteger);
  Payment.Cash_inflow_zak[CIZ_Index].N_Zak := q.Fields[fld.CIZ_N_ZAK].AsString;
  Payment.Cash_inflow_zak[CIZ_Index].D_Zak :=
    q.Fields[fld.CIZ_D_Zak].AsDateTime;

  Payment.Cash_inflow_zak[CIZ_Index].FIO_KLIENT := FIO_Total
    (q.Fields[fld.FIO_SURNAME].AsString,
    q.Fields[fld.FIO_NAME].AsString,
    q.Fields[fld.FIO_PATRONYMIC].AsString).Total;
  if Payment.Cash_inflow_zak[CIZ_Index].FIO_KLIENT='' then
    begin
      Payment.Cash_inflow_zak[CIZ_Index].FIO_KLIENT :=
        q.Fields[fld.NMKLIENT].AsString;
    end;

  Payment.Cash_inflow_zak[CIZ_Index].Canceled := q.Fields[fld.ID_Canceled].AsInteger>0;
  Payment.Cash_inflow_zak[CIZ_Index].Sent_to_personal_Account :=
    q.Fields[fld.CIZ_SENT_TO_PERSONAL_ACCOUNT].AsInteger = 1;

  Payment.ID_CASHONDELIVERY := q.Fields[fld.ID_CASHONDELIVERY].AsInteger;
  // Payment.TYPE_OF_PAYMENT := q.Fields[fld.TYPE_OF_PAYMENT].AsInteger;

  if PayPostal_Load then
  begin
    if Payment.Postal = nil then
      Payment.Postal := PayPostal_Create()
    else
      PayPostal_Clear(Payment.Postal^);

    Payment.Postal.INDEX_PRIEM := q.Fields[fld.POST_Index_Priem].AsString;
    Payment.Postal.INPO.IDC_POSTALITEM := q.Fields[fld.POST_IDC_POSTALITEM].AsString;
    Payment.Postal.KGP_PRIEM := q.Fields[fld.POST_KGP_PRIEM].AsString;
  end;

end;

procedure CASH_inflow_Save(TRW: TIBTransaction; var Payment: TCash_OD);
var
  I: Integer;
  iu: TInsUpd;
begin
  // TODO -cMM: CASH_inflow_Save default body inserted

  // ���������� ���, ���� �� ����
  if (Payment.Cash_inflow.FIO_ID > 0) or
    (Length(Payment.Cash_inflow.FIO.Total) > 0) then
    FIO_Save(TRW, Payment.Cash_inflow.FIO_ID, Payment.Cash_inflow.FIO);


  if not
{$IFDEF VER150}
  UM_Address_GetEmpty(Payment.Cash_inflow.Address)
{$ELSE}
  Payment.Cash_inflow.Address.Empty
{$ENDIF}
  or (Payment.Cash_inflow.Address.ID_Address > 0) then
    Save_KLIENT_ADDRESS_FREE(TRW, nil, Payment.Cash_inflow.Address, True);

  // ���� ��� �������
  Payment.Cash_inflow.D_RECEIPT := DateOf(Payment.Cash_inflow.D_RECEIPT);

  if Payment.Cash_inflow.PAYMENT_DAYS_ID = 0 then
  begin
    Payment.Cash_inflow.PAYMENT_DAYS_ID := ID_PAYMENTDAYS(TRW,
      Payment.Cash_inflow.D_RECEIPT, Payment.Cash_inflow.TYPE_OF_PAYMENT,
      Payment.Cash_inflow.RECIEVER_ID);
    if Payment.Cash_inflow.PAYMENT_DAYS_ID = 0 then
      Payment.Cash_inflow.PAYMENT_DAYS_ID := Get_New_ID_PAYMENT_DAYS(TRW,
        Payment.Cash_inflow.D_RECEIPT, Payment.Cash_inflow.TYPE_OF_PAYMENT,
        Payment.Cash_inflow.RECIEVER_ID, 0);
    // Payment.ID_PAYMENT_DAYS := Payment.Cash_income.PAYMENT_DAYS_ID;
  end;

  iu := TInsUpd.Create(nil, TRW, kiuInsertUpdate, 'CASH_INFLOW');
  try
    if Payment.Cash_inflow.CASH_INFLOW_ID = 0 then
    begin
      Payment.Cash_inflow.CASH_INFLOW_ID := GetValueFieldInt(TRW,
        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_CASH_INFLOW_ID'')');
      if (Payment.ID_CASHONDELIVERY > 0) then
        begin
          iu.TableName := 'STAT_CASHONDELIVERY';
          iu.knd_IU := kiuUpdate;
          iu.AddWhere('ID_CASHONDELIVERY', Payment.ID_CASHONDELIVERY);
          iu.ValueInt('CASH_INFLOW_ID', Payment.Cash_inflow.CASH_INFLOW_ID);
          iu.ExecSQL;
        end;
    end;

    iu.TableName := 'CASH_INFLOW';
    iu.knd_IU := kiuInsertUpdate;
    iu.AddWhere('CASH_INFLOW_ID', Payment.Cash_inflow.CASH_INFLOW_ID);

    iu.ValueInt('TYPE_OF_PAYMENT', Payment.Cash_inflow.TYPE_OF_PAYMENT);
    iu.ValueDate('D_PAYMENT', Payment.Cash_inflow.D_PAYMENT);
    iu.ValueDate('D_RECEIPT', Payment.Cash_inflow.D_RECEIPT);
    iu.ValueInt('N_DOC', Payment.Cash_inflow.N_DOC);
    iu.ValueCurr('AMOUNT', Payment.Cash_inflow.AMOUNT);
    iu.ValueInt('FIO_ID', Payment.Cash_inflow.FIO_ID);
    iu.ValueStr('FIO_KLIENT', Payment.Cash_inflow.FIO_KLIENT);
    iu.ValueInt('ADDRESS_FREE_ID', Payment.Cash_inflow.Address.ID_Address);
    iu.ValueInt('RECIEVER_ID', Payment.Cash_inflow.RECIEVER_ID);
    iu.ValueStr('DETAILS_OF_PAYMENT', Payment.Cash_inflow.DETAILS_OF_PAYMENT);
    iu.ValueInt('PAYMENT_DAYS_ID', Payment.Cash_inflow.PAYMENT_DAYS_ID);
    iu.ValueInt('PAYER_INFO_ID', Payment.Cash_inflow.Contragent.CASH_INFLOW_CUSTOM_ID, False, True);
//    iu.ValueInt('SECTION', Payment.Cash_inflow.Section);
    iu.ValueBol('CLOSED', Payment.Cash_inflow.Closed);

    iu.ExecSQL;

    iu.TableName := 'CASH_INFLOW_ZAK';
{$IF DEBUG_CIZ=0}
    if Payment.Cash_inflow_zak.CASH_INFLOW_ZAK_ID = 0 then
      Payment.Cash_inflow_zak.CASH_INFLOW_ZAK_ID := GetValueFieldInt(TRW,
        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_CASH_INFLOW_ZAK_ID'')');
    iu.AddWhere('CASH_INFLOW_ZAK_ID',
      Payment.Cash_inflow_zak.CASH_INFLOW_ZAK_ID);

    iu.ValueInt('CASH_INFLOW_ID', Payment.Cash_income.CASH_INFLOW_ID);
    iu.ValueCurr('Payment_sum', Payment.Cash_inflow_zak.Payment_sum);
    iu.ValueInt('ID_zak', Payment.Cash_inflow_zak.ID_zak);
    iu.ExecSQL;
{$ELSE}
    for I := 0 to High(Payment.Cash_inflow_zak) do
    begin
      Cash_inflow_zak_Save(TRW, Payment.Cash_inflow_zak[I],
        Payment.Cash_inflow.CASH_INFLOW_ID);

      (*
        if Payment.Cash_inflow_zak[I].CASH_INFLOW_ZAK_ID=0 then
        Payment.Cash_inflow_zak[I].CASH_INFLOW_ZAK_ID := GetValueFieldInt(TRW,
        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_CASH_INFLOW_ZAK_ID'')');
        IU.AddWhere('CASH_INFLOW_ZAK_ID', Payment.Cash_inflow_zak[I].CASH_INFLOW_ZAK_ID);

        IU.ValueInt('CASH_INFLOW_ID',     Payment.Cash_inflow.CASH_INFLOW_ID);
        IU.ValueCurr('Payment_sum',       Payment.Cash_inflow_zak[I].Payment_sum);
        IU.ValueInt('ID_zak',             Payment.Cash_inflow_zak[I].ID_zak);
        IU.ExecSQL;
        *)
    end;
{$IFEND}
  finally
    iu.Free;
  end;
  CASH_INFLOW_Sections_Save(TRW, Payment.Cash_inflow);
end;

procedure Get_CASH_INFLOW_CUSTOM_ID(TRR: TIBTransaction; var CIC: TCash_Inflow_Custom);
begin
  if (CIC.CASH_INFLOW_CUSTOM_ID = 0) and (CIC.CUSTOM_INN <> '') then
    CIC.CASH_INFLOW_CUSTOM_ID := GetValueFieldInt(TRR,
      'Select cic.CASH_INFLOW_CUSTOM_ID from CASH_INFLOW_CUSTOM cic Where cic.CUSTOM_INN=:CUSTOM_INN and cic.CUSTOM_KPP=:CUSTOM_KPP and cic.CUSTOM_BIK=:CUSTOM_BIK and cic.CUSTOM_RS=:CUSTOM_RS',
      ['CUSTOM_INN','CUSTOM_KPP','CUSTOM_BIK','CUSTOM_RS'],
      [CIC.CUSTOM_INN,CIC.CUSTOM_KPP, CIC.CUSTOM_BIK, CIC.CUSTOM_RS]);


end;

function Cash_inflow_zak_add(var Payment: TCash_OD;
  const Payment_sum: Currency; const ID_zak: Integer = 0;
  const CASH_INFLOW_ZAK_ID: Integer = 0): Integer;
begin
  // TODO -cMM: Cash_inflow_zak_add default body inserted
{$IF DEBUG_CIZ=0}
  Result := -1;
  Payment.Cash_inflow_zak.Payment_sum := Payment_sum;
  Payment.Cash_inflow_zak.ID_zak := ID_zak;
  Payment.Cash_inflow_zak.CASH_INFLOW_ZAK_ID := CASH_INFLOW_ZAK_ID;
{$ELSE}
  Result := Length(Payment.Cash_inflow_zak);
  SetLength(Payment.Cash_inflow_zak, Result + 1);
  Payment.Cash_inflow_zak[Result].Payment_sum := Payment_sum;
  Payment.Cash_inflow_zak[Result].ID_zak := ID_zak;
  Payment.Cash_inflow_zak[Result].CASH_INFLOW_ZAK_ID := CASH_INFLOW_ZAK_ID;
{$IFEND}
end;

procedure Cash_inflow_zak_Save(TRW: TIBTransaction; var ACIZ: TCash_inflow_zak;
  const CASH_INFLOW_ID: Integer);
var
  iu: TInsUpd;
begin
  // TODO -cMM: Cash_inflow_zak_Save default body inserted
  iu := TInsUpd.Create(nil, TRW, kiuInsertUpdate, 'CASH_INFLOW_ZAK');
  try
    if ACIZ.CASH_INFLOW_ZAK_ID = 0 then
      ACIZ.CASH_INFLOW_ZAK_ID := GetValueFieldInt(TRW,
        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_CASH_INFLOW_ZAK_ID'')');
    iu.AddWhere('CASH_INFLOW_ZAK_ID', ACIZ.CASH_INFLOW_ZAK_ID);
    iu.ValueInt('CASH_INFLOW_ID', CASH_INFLOW_ID);
    iu.ValueCurr('Payment_sum', ACIZ.Payment_sum);
    iu.ValueInt('ID_zak', ACIZ.ID_zak);
    iu.ValueBol('SENT_TO_PERSONAL_ACCOUNT', ACIZ.Sent_to_personal_Account);
    iu.ExecSQL;
  finally
    iu.Free
  end;
end;

procedure CASH_INFLOW_CUSTOM_Save(TRW: TIBTransaction; var ACIC: TCash_Inflow_Custom);
var
  iu: TInsUpd;
begin
  if ACIC.CASH_INFLOW_CUSTOM_ID = 0 then
    ACIC.CASH_INFLOW_CUSTOM_ID := GetValueFieldInt(TRW,
        'select KeyOut from MAKE_UNIQUE_GEN(''GEN_CASH_INFLOW_CUSTOM_ID'')');

  iu := TInsUpd.Create(nil, TRW, kiuInsertUpdate, 'CASH_INFLOW_CUSTOM');
  try
    iu.AddWhere('CASH_INFLOW_CUSTOM_ID', ACIC.CASH_INFLOW_CUSTOM_ID);
    iu.ValueStr('CUSTOM_S', ACIC.CUSTOM_S, True, True);
    iu.ValueStr('CUSTOM_NAME', ACIC.CUSTOM_NAME, True, True);
    iu.ValueStr('CUSTOM_INN', ACIC.CUSTOM_INN, True);
    iu.ValueStr('CUSTOM_KPP', ACIC.CUSTOM_KPP, True);
    iu.ValueStr('CUSTOM_RS', ACIC.CUSTOM_RS, True);
    iu.ValueStr('CUSTOM_BANK1', ACIC.CUSTOM_BANK1, True, True);
    iu.ValueStr('CUSTOM_BIK', ACIC.CUSTOM_BIK, True);
    iu.ValueStr('CUSTOM_KS', ACIC.CUSTOM_KS, True, True);

    iu.ExecSQL;
  finally
    iu.Free;
  end;


end;
{$IFDEF VER150}
// DELPHI_7
function CIZ_IS_all_ID_Zak(const Cash_inflow_zak: TaCash_inflow_zak): Boolean;
{$ELSE}
  function TCash_OD.GetIS_all_ID_Zak: Boolean;
{$ENDIF}
  var
    I: Integer;
  begin
    // TODO -cMM: TCash_OD.GetIS_all_ID_Zak default body inserted
    Result := Length(Cash_inflow_zak) > 0;
    for I := 0 to High(Cash_inflow_zak) do
      if Cash_inflow_zak[I].ID_zak = 0 then
      begin
        Result := False;
        Break;
      end;
  end;
{$IFDEF VER150} // DELPHI_7
  function CIZ_IS_any_ID_Zak(const Cash_inflow_zak: TaCash_inflow_zak): Boolean;
{$ELSE}
    function TCash_OD.GetIS_any_ID_Zak: Boolean;
{$ENDIF}
    var
      I: Integer;
    begin
      // TODO -cMM: TCash_OD.GetIS_any_ID_Zak default body inserted
      Result := False;
      for I := 0 to High(Cash_inflow_zak) do
        if Cash_inflow_zak[I].ID_zak > 0 then
        begin
          Result := True;
          Break;
        end;
    end;
{$IFDEF VER150} // DELPHI_7
    function IndexCIZ_By_Id(const Cash_inflow_zak: TaCash_inflow_zak;
      const CIZ_Id: Integer): Integer;
{$ELSE}
      function TCash_OD.IndexCIZ_By_Id(const CIZ_Id: Integer): Integer;
{$ENDIF}
      var
        I: Integer;
      begin
        Result := -1;
        if CIZ_Id = 0 then
          Exit;

        for I := 0 to High(Cash_inflow_zak) do
          if Cash_inflow_zak[I].CASH_INFLOW_ZAK_ID = CIZ_Id then
          begin
            Result := I;
            Break;
          end;
      end;
{$IFDEF VER150} // DELPHI_7
      function IndexCIZ_ByMainSum(const Cash_inflow: TCash_inflow;
        const Cash_inflow_zak: TaCash_inflow_zak): Integer;
{$ELSE}
        function TCash_OD.IndexCIZ_ByMainSum: Integer;
{$ENDIF}
        var
          I: Integer;
        begin
          // ���� ������� ������� ��� ID_Zak,�� �������� ������������ �� ����� �����
          Result := -1;
          for I := 0 to High(Cash_inflow_zak) do
            if Cash_inflow_zak[I].ID_zak = 0 then
            begin

              if Cash_inflow_zak[I].Payment_sum = Cash_inflow.AMOUNT then
              begin // ����� ����������� ����� ����� ��������, �����
                Result := I;
                Break;
              end;

            end;
        end;
{$IFDEF VER150} // DELPHI_7
        function IndexCIZ_ByZeroId_Zak(const Cash_inflow: TCash_inflow;
          const Cash_inflow_zak: TaCash_inflow_zak): Integer;
{$ELSE}
          function TCash_OD.IndexCIZ_ByZeroId_Zak: Integer;
{$ENDIF}
          var
            I: Integer;
          begin
            // ���� ������� ������� ��� ID_Zak,
            // ������������ �������� �������� ������������ �� �����
            Result := -1;
            for I := 0 to High(Cash_inflow_zak) do
              if Cash_inflow_zak[I].ID_zak = 0 then
              begin

                if Cash_inflow_zak[I].Payment_sum = Cash_inflow.AMOUNT then
                begin // ����� ����������� ����� ����� ��������, �����
                  Result := I;
                  Break;
                end
                else if Result = -1 then
                  Result := I; // ����� ������ �� ������� ���� ���

              end;
          end;
{$IFDEF VER150} // DELPHI_7
          function CIZ_ID_ZAK(const Cash_inflow_zak: TaCash_inflow_zak;
            const Index: Integer): Integer;
{$ELSE}
            function TCash_OD.GetCIZ_ID_Zak(const Index: Integer): Integer;
{$ENDIF}
            begin
              if (index >= 0) and (index <= High(Cash_inflow_zak)) then
                Result := Cash_inflow_zak[Index].ID_zak
              else
                Result := 0;
            end;
{$IFDEF VER150} // DELPHI_7
            function CIZ_Is_ID_Zak(const Cash_inflow_zak: TaCash_inflow_zak;
              const ID_zak: Integer): Boolean;
{$ELSE}
              function TCash_OD.Is_ID_Zak(const ID_zak: Integer): Boolean;
{$ENDIF}
              var
                I: Integer;
              begin
                Result := False;
                for I := 0 to High(Cash_inflow_zak) do
                  if (Cash_inflow_zak[I].ID_zak = ID_zak) then
                  begin
                    Result := True;
                    Break;
                  end;
              end;
function Get_ID_from_Note(const ANote: string): Integer;
var
  id_order: Integer;
  P, Start: PChar;
  S: string;
begin
  Result := 0;
  P := PChar(ANote);
  if P <> nil then
    while (P^ <> #0) and (Result = 0) do
      begin
        Start := P;
        while not {$IFDEF VER150} (P^ in [' ','-',',', #0]) {$ELSE} CharInSet(P^, [' ','-',',', #0]) {$ENDIF VER150}

            do Inc(P);

        SetString(S, Start, P - Start);
        if ((Length(S) in [6,7]) and IsDigital(S[1])) or
           ((Length(S) in [7]) and IsDigital(S[2]) and
           {$IFDEF VER150} (S[1] in ['N', '�']) {$ELSE} CharInSet(S[1], ['N', '�']) {$ENDIF VER150}
           )
           then
          begin
            if {$IFDEF VER150} (S[1] in ['N', '�']) {$ELSE} CharInSet(S[1], ['N', '�']){$ENDIF VER150} then
              Delete(S,1,1);
            id_order := Strtointdef(s,0);
            if id_order >= 270000 then
              begin
                Result := id_order;
              end;
          end;

        if {$IFDEF VER150} (P^ in [' ','-',',']) {$ELSE}CharInSet(P^, [' ','-',',']){$ENDIF VER150} then Inc(P);

      end;

end;
function Get_ID_Order_from_Note(const ANote: string): Integer;
const
  a: array [0 .. 22] of string = (
    'N ���.',
    '����� ( ������������ �����������)',
    '�����',
    '������ ������ ������',
    '������ ������ �����',
    '������ ������ ���.',
    '������ ������',
    '������ �����',
    '������',
    '�� �����',
    '������ �� ��.',
    '������ �� ��',
    '����� �����',
    '������',
    '������',
    '�����',
    '���',
    '��������',
    '�-��',
    '�����: N',
    '������',
    '���_���:',
    '�/�');
var
  s, s1, sID_Order: string;
  t, I, i2: Integer;
begin
  Result := 0;
  if ANote = '' then
    Exit;


  sID_Order := '';

  for I := 0 to High(a) do
  begin
    s1 := a[I];
    s := AnsiUpperCase(ANote);

    t := Pos(s1, s);
    if t > 0 then
    begin
      Delete(s, 1, t + Length(s1) - 1);
      s := trim(s);
      if s = '' then
        Continue;

      while (Length(s) >= 1) and (
        {$IFDEF VER150} (S[1] in ['N', '�', ' ', '"','/','.']) {$ELSE} CharInSet(s[1], ['N', '�', ' ', '"','/','.']) {$ENDIF VER150}

        or (s[1] = chr(8470))) do
        Delete(s, 1, 1);

      if s = '' then
        Continue;

      i2 := 1;

      while (i2<=Length(s)) and IsDigital(s[i2]) do
      begin
        sID_Order := sID_Order + s[i2];
        inc(i2);
      end;
      if sID_Order <> '' then
        Result := StrToIntDef(sID_Order, 0);

    end;

    if Result > 0 then
      Exit;

  end;

  if (Result = 0) and (Length(s) = 6) then
  begin
    i2 := 1;

    while (i2 <= Length(s)) and IsDigital(s[i2]) do
    begin
      sID_Order := sID_Order + s[i2];
      inc(i2);
    end;
    if sID_Order <> '' then
      Result := StrToInt(sID_Order);
  end;

  if Result > 0 then Exit;
  // ��������� �������, ���� ������������ ����� ����� ������
  Result := Get_ID_from_Note(ANote);
end;

function Cash_Copy(const Payment: TCash_OD): TCash_OD;
begin
  // TODO -cMM: Cash_Copy default body inserted
  Cash_OD_Clear(Result);
  Result.ID_CASHONDELIVERY := Payment.ID_CASHONDELIVERY;
  Result.ID_ZAK := Payment.ID_ZAK;
  Result.D_SendUchmag := Payment.D_SendUchmag;

  if Assigned(Payment.Postal) then
    begin
      Result.Postal := PayPostal_Create;
      Result.Postal^ := PayPostal_Copy(Payment.Postal);
    end;



//    Address1: TUM_Address;
  Result.Cash_inflow:= Payment.Cash_inflow;
end;

function PayPostal_Copy(const Postal: PPayPostal): TPayPostal;
begin
  // TODO -cMM: PayPostal_Copy default body inserted

  Result.KGP_PRIEM := Postal.KGP_PRIEM;
  Result.RPO := Postal.RPO;
  Result.INDEX_PRIEM := Postal.INDEX_PRIEM;
  Result.INPO.IDC_POSTALITEM := Postal.INPO.IDC_POSTALITEM;
  Result.ID_File := Postal.ID_File;
end;

function JSON_SaveCIZ(const CIZ: TCash_inflow_zak;
      const Path_OutUchMag: string; AName,AValue: array of string): string;
var
  p:TParams;
  I: Integer;
begin
  p := TParams.Create;
  try
    if CIZ.ID_zak > 0 then
      p.SetValue('ID_ZAK',CIZ.ID_zak);
    if CIZ.N_Zak<>'' then
      p.SetValue('N_ZAK',CIZ.N_Zak);
    p.SetValue('PAYMENT_SUM',CIZ.Payment_sum);
    if CIZ.FIO_Klient<>'' then
      p.SetValue('FIO_Klient',CIZ.FIO_Klient);
//    p.SetValue('Type_Edit',Type_Edit);

    for I := 0 to high(AName) do
      p.SetValue(AName[I], AValue[I]);

    if Path_OutUchMag <> '' then
      begin
        p.SetValue('Path_OutUchMag', Path_OutUchMag);
        p.SetValue('Path_OutUchMag_Access', BoolToStr_YN(DirectoryExists(Path_OutUchMag)));
      end;
    Result:=p.Text_asJSON;
  finally
    p.Free;
  end;

end;

procedure CHANGE_CASH_INFLOW_ADD_PAYMENT(TRW: TLibTransaction; var Payment:
    TCash_OD);
var
  q: TIBSQL;
begin
  q := TIBSQL.Create(nil);
  try
    TRW.Active := True;
    q.Transaction := TRW;
    q.SQL.Text := 'Select * from CHANGE_CASH_INFLOW_ADD_PAYMENT(:CASH_INFLOW_ID)';
    q.ParamByName('CASH_INFLOW_ID').AsInteger := Payment.Cash_inflow.CASH_INFLOW_ID;
    q.ExecQuery;
    if q.RecordCount > 0 then
      begin
        if (q.FieldByName('NEW_SECTION').AsString <> IntArrayToString(Payment.Cash_inflow.Sections)) or
           (q.FieldByName('NEW_CLOSED').AsInteger <> Integer(Payment.Cash_inflow.Closed)) then
          begin
            if not ContainsItem(Payment.Cash_inflow.Sections,
                                q.FieldByName('NEW_SECTION').AsInteger) then
              AddIntToArray(Payment.Cash_inflow.Sections,
                            q.FieldByName('NEW_SECTION').AsInteger);
            Payment.Cash_inflow.Closed := q.FieldByName('NEW_CLOSED').AsInteger=1;
          end;
      end;
  finally
    q.Free;
    if Length(Payment.Cash_inflow.Sections) = 0 then
      begin
        AddIntToArray(Payment.Cash_inflow.Sections, 0);
        CASH_INFLOW_Sections_Save(TRW, Payment.Cash_inflow);
      end;
  end;

end;

procedure CASH_INFLOW_Sections_Save(TRW: TIBTransaction; const Cash_inflow:
    TCash_inflow);
var
  q: TIBSQL;
  I: Integer;
  iu: TInsUpd;
  Sections2: TIntegerDynArray;
begin
  // TODO -cMM: CASH_INFLOW_Sections_Save default body inserted
  q := TIBSQL.Create(nil);
  try
    q.Transaction := TRW;
    q.SQL.Text := 'Select * from CASH_INFLOW_IN_SECTION where CASH_INFLOW_ID=:CASH_INFLOW_ID';
    q.ParamByName('CASH_INFLOW_ID').AsInteger := Cash_inflow.CASH_INFLOW_ID;
    q.ExecQuery;
    while not q.Eof do
    begin
      I := IndexOfIntArray(Cash_inflow.Sections,
        q.FieldByName('SECTION_ID').AsInteger);
      if I = -1 then
        begin
          //� ������ � ������� ����������, � ��� �������
          Delete_in_table(TRW, 'CASH_INFLOW_IN_SECTION',
            'CASH_INFLOW_IN_SECTION_ID',
            q.FieldByName('CASH_INFLOW_IN_SECTION_ID').AsInteger)
        end
      else
        begin
          //������� �� ������, ���������� ����� ���������
          AddIntToArray(Sections2, Cash_inflow.Sections[I]);
        end;
      q.Next;
    end;

    for I := 0 to High(Cash_inflow.Sections) do
      if not ContainsItem(Sections2, Cash_inflow.Sections[I]) then
        begin
          iu := TInsUpd.Create(nil, TRW, kiuInsert, 'CASH_INFLOW_IN_SECTION');
          try
            iu.ValueInt('CASH_INFLOW_ID', Cash_inflow.CASH_INFLOW_ID);
            iu.ValueInt('SECTION_ID', Cash_inflow.Sections[I]);
            iu.ExecSQL;
          finally
            iu.Free;
          end;
        end;

  finally
    q.Free;
    SetLength(Sections2,0);
  end;
end;

function Payment_ImageIndex(const TYPE_OF_PAYMENT: Integer): Integer;
begin
  //
  case TYPE_OF_PAYMENT of
  TOP_NONE,TOP_NalPay:  Result := 1;  // �����
  TOP_SBERBANK_UCH,TOP_SBERBANK_KANC:  Result := 0;  // ����
  TOP_PM: Result := 2;  // WM
  else
      Result := -1;
  end;
end;

initialization

finalization

// ������� ������ �������� �����
SetLength(ATYPE_OF_PAYMENT, 0);

end.
