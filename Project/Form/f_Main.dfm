object fMain: TfMain
  Left = 0
  Top = 0
  Caption = 'fMain'
  ClientHeight = 289
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 41
    Align = alTop
    TabOrder = 0
    object pnl1: TPanel
      Left = 368
      Top = 1
      Width = 185
      Height = 39
      Align = alRight
      TabOrder = 0
    end
    object pnl2: TPanel
      Left = 1
      Top = 1
      Width = 367
      Height = 39
      Align = alClient
      TabOrder = 1
      object tlb1: TToolBar
        Left = 1
        Top = 1
        Width = 365
        Height = 36
        ButtonHeight = 38
        ButtonWidth = 39
        Caption = 'tlb1'
        Images = dmGUI.il32
        TabOrder = 0
        object btn1: TToolButton
          Left = 0
          Top = 0
          Caption = 'btn1'
          ImageIndex = 43
        end
        object btnReestrUchmag: TToolButton
          Left = 39
          Top = 0
          Caption = 'btnReestrUchmag'
          ImageIndex = 2
        end
        object btn2: TToolButton
          Left = 78
          Top = 0
          Caption = 'btn2'
          ImageIndex = 3
          OnClick = btn2Click
        end
      end
    end
  end
end
