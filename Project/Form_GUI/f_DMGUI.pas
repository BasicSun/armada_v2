unit f_DMGUI;

interface

uses
  SysUtils, Classes, ImgList, Controls, Graphics;

type
  TdmGUI = class(TDataModule)
    il16: TImageList;
    il32: TImageList;
    ilEdit16: TImageList;
    ilSmartfilter: TImageList;
    il_Delivery16: TImageList;
    il_Payment16: TImageList;
  private
    { Private declarations }
  public
    { Public declarations }
    function Delivery_ImageIndex(const ID_DELIVERY: Integer):Integer;

    procedure Draw32(const ImageIndex:Integer; Glyph: TBitmap);
  end;

var
  dmGUI: TdmGUI;
const
  IMG_book_blue   =38;
  IMG_book_green  =39;
  IMG_book_yellow =40;
  IMG_book_red    =41;

  STAR_green       =44;
  STAR_green_empty =57;
  STAR_red         =43;
  STAR_yellow      =47;

  //галочки
  IMG_check_green      =42;
  IMG_check_lightblue  =60;

  //Восклицательный знак
  ExclamationPoint = 49;
  ExclamationPoint2 = 75;
  ExclamationPointOrange = 85;

implementation

uses Types;

{$R *.dfm}

{ TdmGUI }

function TdmGUI.Delivery_ImageIndex(const ID_DELIVERY: Integer): Integer;
begin
  case ID_DELIVERY of
    2   : Result := 0;
    8,
    9   : Result := 1;
    10,
    11  : Result := 3;
    14  : Result := 4;
    15..
    23  : Result := 5;
    13  : Result := 6;
    883 : Result := 15;
    24  : Result := 7;
    25  : Result := 8;
    26  : Result := 9;
    268 : Result := 10;
    1871: Result := 14; // B2C
    261 : Result := 11;
    275 : Result := 12;
    1213: Result := 16;
  else
    Result := -1;
  end;
end;

procedure TdmGUI.Draw32(const ImageIndex: Integer; Glyph: TBitmap);
var
  Rect : TRect;
begin
  Rect.Top := 0; Rect.Left := 0; Rect.Right:= 32; Rect.Bottom := 32;
  Glyph.Canvas.FillRect(Rect);
  il32.Draw(Glyph.Canvas, 0,0,ImageIndex);
end;



end.
